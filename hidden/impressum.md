---
permalink: /impressum/
---

<img src="{{ "/assets/images/aktionsbuendnis_brandenburg.jpg" | relative_url }}" alt="Aktionsbündnis Brandenburg gegen Gewalt, Rechtsextremismus und Fremdenfeindlichkeit" class="w-25 float-right">

# Impressum

### Aktionsbündnis gegen Gewalt, Rechtsextremismus und Fremdenfeindlichkeit

Mittelstraße 38/39

14467 Potsdam

**Vertreten durch:**

Frauke Büttner, Leiterin der Geschäftsstelle

**Kontakt:**

E-Mail: [kontakt@aktionsbuendnis-brandenburg.de](mailto:kontakt@aktionsbuendnis-brandenburg.de)

Telefon: 0331 505824-25

Fax: 0331 505824-29

**Verantwortlich für den Inhalt nach § 55 Abs. 2 RstV:**

Frauke Büttner

**Programmierung, Konzept & Gestaltung:**

SpreeBytes UG [spreebytes.net](http://www.spreebytes.net/)

**Rechtliche Hinweise**

Rechtsrelevante Schriftsätze müssen per Telefax oder auf dem Postweg eingereicht werden. Die Kommunikation per E-Mail ist nicht rechtswirksam.

### Förderung

<img src="{{ "/assets/images/steine/MWFK_4C_1_.png" | relative_url }}" alt="Logo MWFK" class="w-25">

Gefördert mit Mitteln des Ministeriums für Wissenschaft, Forschung und Kultur des Landes Brandenburg

### Inhalt des Angebots

Die Inhalte unserer Webseite wurden sorgfältig erstellt. Das Aktionsbündnis gegen Gewalt, Rechtsextremismus und Fremdenfeindlichkeit (Aktionsbündnis) übernimmt keine Gewähr für die Aktualität, Richtigkeit, Vollständigkeit oder Qualität der bereitgestellten Informationen.

Haftungsansprüche, die sich auf Schäden materieller oder ideeller Art beziehen, welche durch die Nutzung oder Nichtnutzung der dargebotenen Informationen beziehungsweise durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind ausgeschlossen, wenn kein vorsätzliches oder grob fahrlässiges Verschulden des Aktionsbündnisses nachgewiesen werden kann.

Eine diesbezügliche Haftung ist erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.

### Links zu externen Webseiten

Das Angebot des Aktionsbündnisses enthält Links zu externen Webseiten, auf deren Inhalte kein Einfluss besteht. Für diese fremden Inhalte übernimmt das Aktionsbündnis keine Gewähr. Für die Inhalte der verlinkten Seiten sind die jeweiligen Anbieterinnen und Anbieter oder die Betreiberinnen und Betreiber der Seiten verantwortlich.

Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.

Das Aktionsbündnis distanziert sich von allen Inhalten aller gelinkten Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links. Erst wenn das Aktionsbündnis feststellt oder von anderen darauf hingewiesen wird, dass ein Angebot, zu dem das Aktionsbündnis einen Link bereitgestellt hat, eine zivil- oder strafrechtliche Verantwortlichkeit auslöst, wird der Verweis auf dieses Angebot aufgehoben, soweit dies technisch möglich und zumutbar ist. Die technische Möglichkeit und Zumutbarkeit wird nicht dadurch beeinflusst, dass auch nach der Unterbindung des Zugriffs von der Webseite [http://www.stolpersteine-brandenburg.de](https://www.stolpersteine-brandenburg.de/) von anderen Servern aus auf das rechtswidrige oder strafbare Angebot zugegriffen werden kann. Dies gilt auch für Proxy-Server und Suchmaschinen mit Spiegel-Funktion.

### Urheber- und Kennzeichenrecht

Das Aktionsbündnis versucht, die Urheberrechte der verwendeten Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, vom Aktionsbündnis selbst erstellte Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen.

Sollte sich auf den jeweiligen Seiten dennoch eine ungekennzeichnete, aber durch fremdes Copyright geschützte Grafik, ein Tondokument, eine Videosequenz oder ein Text befinden, so konnte das Copyright nicht festgestellt werden. Im Falle einer solchen unbeabsichtigten Copyrightverletzung wird das Aktionsbündnis nach Benachrichtigung das entsprechende Objekt aus seiner Publikation entfernen beziehungsweise mit dem entsprechenden Copyright kenntlich machen.

Das Copyright für veröffentlichte, vom Aktionsbündnis selbst erstellte Objekte bleibt allein bei den Autorinnen und Autoren der Webseiten. Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen oder Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des Aktionsbündnisses nicht gestattet. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet, es sei denn, das Copyright in einzelnen Publikationen sieht anderes vor.
