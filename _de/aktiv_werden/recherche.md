---
title: Recherche
category: 2 aktiv werden
order: 40
---

# Recherche

<img src="{{ "/assets/images/steine/stolpersteine-bei-der-verlegung.jpg" | relative_url }}" alt="Gunter Demnig bei der Verlegung" class="w-50 float-right ml-3 mt-3">

Am Anfang Ihres Stolpersteinprojektes steht die Forschung. Beachten Sie, dass es sich bei den Stolpersteinen um ein Kunstprojekt handelt, das sich mit dem aktiven Gedenken beschäftigt. Die historische Recherche sollte über die Datenerfassung für den Stein hinausgehen und so das Leben des Menschen wieder in Erinnerung rufen. Ein bloßes Abschreiben der Angaben aus Gedenkbüchern ist nicht in diesem Sinne.

Versuchen Sie, einen nachbarschaftlichen Kommunikationsprozess zu initiieren. Treten Sie an ehemalige und jetzige Bewohner, örtliche Vereine, religiöse Gemeinden, Parteien, Schulen, Ausbildungsbetriebe, Hochschulen und Universitäten heran. Suchen Sie Kontakt zu Heimatforschern und Wissenschaftlern historischer Fakultäten.

Erfragen Sie:

*   **Personen:** Familienangehörige, Freund\*innen, Klassenkamerad\*innen, Arbeitskolleg\*innen
*   **Wohnorte:** Straße, Wohnung im Haus, weitere Adressen bei Umzügen, Veränderungen im Stadtbild
*   **Familie:** Geburten, Taufen, Hochzeiten, Jubiläen, Todesfälle, Namensänderungen
*   **Ausbildung und Beruf:** Schul- und Berufsabschlüsse, berufliche Tätigkeiten, Studium
*   **Ereignisse:** Veränderungen nach 1933, Reaktion im Ort auf nationalsozialistische Politik

Sie sollten bei der Recherche zunächst vor Ort beginnen und dann weiterführend suchen. Befragen Sie wenn möglich Zeitzeug\*innen oder deren Nachfahren. Folgendes Vorgehen ist denkbar:

*   **Ermittlung von Personen und Familien, für die Stolpersteine verlegt werden sollen:** Wenn die Namen und der Wohnort bereits bekannt sind, konzentrieren sich die Nachforschungen auf weitere Familienmitglieder oder Hausbewohner\*innen. Es ist aber auch möglich, über die Erforschung der Geschichte eines oder mehrere Häuser während der NS-Zeit ein Stolpersteinprojekt zu initiieren. Auf der Webseite des Bundesarchivs (<http://www.bundesarchiv.de/gedenkbuch>) finden Sie die Namen von jüdischen Verfolgten. Hier können Sie vom Namen oder Wohnort ausgehend recherchieren. Dort werden jedoch keine konkreten Adressen angegeben. Zugang zu den Akten und den Adressen erhalten Sie dann über das Brandenburgische Landeshauptarchiv oder über örtliches Archive oder Museen.
*   **Ermittlung vor Ort:** Suchen Sie zunächst nach Informationen in der unmittelbaren Umgebung des Wohnortes. Fragen Sie nach Familienangehörigen, Zeitzeug\*innen und ehemaligen Bewohner\*innen des Hauses und der Straße.
*   **Recherche in öffentlich zugänglichen Publikationen:** Wenden Sie sich an Heimatmuseen, Geschichtsvereine sowie die Stadt-, Landes- und Staatsbibliothek.
*   **Online-Recherche:** Beginnen Sie beim Befragen von Suchmaschinen. Schauen Sie in die Online-Kataloge der Bibliotheken, vor allem in die Zeitschriftensammlungen oder auch in die online verfügbaren Materialien. Suchen Sie personenbezogen nach Adressen und Ansprechpartner\*innen.
*   **Recherche in Archiven**
*   **Örtliche Archive:** Versuchen Sie zunächst, vor Ort Dokumente zu finden. Durchforschen Sie beispielsweise Materialien der Stadtverwaltung, der Kirchengemeinde, der Standesämter und von Geschichtsvereinen.

> Auf der Seite [Archive und Gedenkstätten]({{ "de/hintergrund/archive_und_gedenkstaetten.html" | relative_url }}) sind die Archive mit Adressen aufgeführt.

Sie sind für Ihr Rechercheergebnis und den Umgang mit personenbezogenen Daten selbst verantwortlich. Achten Sie auf Quellennachweise und überprüfen Sie die Daten, die von anderen recherchiert wurden. Das ist einerseits wichtig, um bei späteren Nachfragen antworten zu können. Anderseits ersparen Sie sich nochmaliges Suchen bei der Veröffentlichung ihres Forschungsergebnisses.
