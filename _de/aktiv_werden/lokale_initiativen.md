---
title: Lokale Initiativen
category: 2 aktiv werden
order: 60
---

# Lokale Initiativen

Wir als Koordinierungsstelle der Stolpersteine in Brandenburg stehen jederzeit als Anlaufstelle für Fragen zur Verfügung. In Brandenburg existieren aber bereits zahlreiche Initiativen, ohne die unsere Arbeit nicht möglich wäre. Hier finden sich langjährig Engagierte, die mit Stolpersteinen in Städten und Gemeinden an Verfolgte des Nationalsozialismus erinnern. Auch einige Einzelpersonen haben weitreichende Erfahrungen in diesem Feld gesammelt und können Auskunft darüber geben.

## Initiativen

- [Cottbuser Aufbruch](https://www.cottbuser-aufbruch.de/stolpersteine/wo-sind-die-stolpersteine.html)
- [Stadt Eberswalde](https://geoportal.eberswalde.de/application/stolpersteine)
- [Stolpersteine Falkensee und Osthavelland](http://www.stolpersteine-falkensee.de/)
- [Fehrbellin bleibt bunt](https://www.buendnis-toleranz.de/arbeitsfelder/anlaufstelle/initiativen/173699/fehrbellin-bleibt-bunt)
- [STOLPERSTEINE Frankfurt (Oder) und Słubice](http://www.stolpersteine-ffo.de/)
- [Fürstenwalder Stolpersteine](http://stolpersteine.museum-fuerstenwalde.de/)
- [Naemi Wilke Stift Guben](https://www.naemi-wilke-stift.de/de/stiftung/geschichte.html)
- [Geschichtskreis im Kulturkreis Hohen Neuendorf](https://kulturkreis-hn.de/index.php/geschichtskreis.html)
- [Stolpersteine in Kleinmachnow](http://www.stolpersteine-kleinmachnow.de/)
- [Stadt Luckenwalde](https://www.luckenwalde.de/Stadt/Geschichte/Stolpersteine)
- [Stolpersteine in Liebenberg](http://www.dkb-stiftung.de/project/stolpersteine/)
- [Vorbereitungskreis "Stolpersteine in Neuruppin"](http://www.stolpersteine-neuruppin.de/)
- [Forum gegen Rassismus und rechte Gewalt Oranienburg: Stolpersteine in Oranienburg](http://www.fibb-oranienburg.de/stolpersteine/)
- [Landeshauptstadt Potsdam: Stolpersteine in Potsdam](https://www.potsdam.de/kategorie/aktion-stolpersteine)
- [Arbeitsgemeinschaft „Stolpersteine“ und Stadt Schwedt (Oder)](https://www.schwedt.eu/de/stadtmuseum/juedisches-ritualbad/stolpersteine/392898)
- [Heimatverein Schweizer Haus Seelow e.V.](https://www.heimatverein-seelow.de/)
- [Stadt Wittenberge](https://www.wittenberge.de/seite/65112/stolpersteine.html)
- [Georg-Mendheim-Oberstufenzentrum Zehdenick](http://www.gmosz.de/hf/)

