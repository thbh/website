---
title: Steine Putzen
category: 2 aktiv werden
order: 50
---

# Steine Putzen

<img src="{{ "/assets/images/steine/stolperstein-putzen-hermann-jacks.jpg" | relative_url }}" alt="Stolperstein putzen Hermann Jacks" class="float-right w-25 ml-3 mt-3">

Die Pflege der verlegten Stolpersteine ist ein genauso wichtiger Teil des Gedenkens wie die Recherche der Personendaten. Hier gibt es ein paar Tipps und Hinweise, wie Sie diese Pflege richtig angehen können.
Die Steine sollten regelmäßig gereinigt werden, da sich die Messingplatte mit der Zeit witterungsbedingt verfärben kann und die Inschrift dadurch unleserlich wird. Von zu intensivem Putzen raten einige Initiativen jedoch ab, da hierdurch die Oxidationsschicht beschädigt werden kann und die Gefahr besteht, dass die Inschrift durch den Materialabtrag unlesbar wird.

Für die grobe Reinigung können ein kratzfreier Schwamm und Wasser verwendet werden. Für eine gründlichere Reinigung empfehlen sich handelsübliche Metallreiniger, beispielsweise der Hersteller „Sidol“ oder „Elsterglanz“. Um weiße Ränder auf dem umliegenden Pflaster zu vermeiden, sollte das Reinigungsmittel gering dosiert mit einem Schwamm oder Tuch auf der Messingplatte verteilt werden. Nach einer Einwirkzeit von etwa einer Minute muss die Platte mit einem trockenen Tuch poliert werden. Bei stärkeren Verschmutzungen ist der Vorgang gegebenenfalls zu wiederholen. Benutzen Sie keine Hilfsmittel mit sehr harter Oberfläche wie Drahtbürsten oder Topfkratzer, da sie die Messingplatten beschädigen. Hier finden Sie ein Video mit einer Putzanleitung. Gemeinsame Pflegeaktionen von Initiativen können dazu beitragen, eine Gedenk- und Erinnerungskultur wach zu halten.


[Stolpersteine putzen - Versuch, Irrtum und Erfolg (1:27)](https://www.youtube.com/watch?v=x1BjHU4fBFc)

[Wie putze ich einen Stolperstein? (1:25)](https://www.youtube.com/watch?v=sOa8cTHNhO0)

