---
title: Stolpersteinpatenschaft
category: 2 aktiv werden
order: 20
---

# Stolpersteinpatenschaft

<img src="{{ "/assets/images/steine/stolperstein-familie-joel-putzen.jpg" | relative_url }}" alt="Stolperstein putzen Hermann Jacks" class="float-right w-25 ml-3 mt-3">

Für die Herstellung und Verlegung eines Stolpersteins fallen derzeit Kosten in Höhe von 120 Euro an. Wenn Gunter Demnig die entsprechenden Stolpersteine selbst verlegt, können unter Umständen noch Übernachtungskosten für ihn und seinen Fahrer anfallen, je nachdem wie weit die Anreise ist. Die meisten Steine werden vom Künstler selbst verlegt. Aufgrund der Corona-Pandemie können derzeit keine Verlegungen mit Gunter Demnig stattfinden, weshalb die Steine auch verschickt und selbst verlegt werden können. Falls zusätzlich zu der Verlegung ein Vortrag des Künstlers gewünscht wird, belaufen sich die Honorarkosten auf 200 Euro zuzüglich Mehrwertsteuer.

Um diese Kosten zu tragen, richten viele Stolperstein-Initiativen eigene Konten ein und rufen zu Spenden auf. Es besteht die Möglichkeit, Patenschaften für einzelne Stolpersteine einzurichten. Die Pat\*innen tragen die Kosten der Herstellung und Verlegung eines Stolpersteines. Natürlich können sich auch Patengemeinschaften die Kosten für einen oder mehrere Steine teilen. Um Unterstützung für Ihr Stolperstein-Projekt zu erhalten, können Sie sich an verschiedene lokale Stellen wie örtliche Betriebe, Vereine und Institutionen, Wohnungsbaugesellschaften, Banken oder Stiftungen wenden und versuchen, diese für eine Patenschaft zu gewinnen. Selbstverständlich können auch Privatpersonen eine oder mehrere Patenschaften übernehmen.
