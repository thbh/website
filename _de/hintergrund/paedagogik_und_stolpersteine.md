---
title: Pädagogik und Stolpersteine
category: 3 hintergrund
order: 20
---

# Pädagogik und Stolpersteine

Als dezentrales Mahnmal eignen sich Stolpersteine für vielfältige pädagogische Angebote. Schüler\*innen, Auszubildende oder außerschulische Jugendgruppen können auf unterschiedliche Weise mit Stolpersteinen arbeiten. Diese Website bietet mit dem Verzeichnis der Stolpersteine und der verknüpften Datenbank die Möglichkeit, zu bereits in Brandenburg verlegten Stolpersteinen zu recherchieren und sich mit einzelnen Biografien verfolgter Menschen auseinanderzusetzen. Auf diese Weise können sich die Jugendlichen mit Stolpersteinen und den Biografien der Verfolgten in ihren Wohnorten befassen und Erfahrungen in der historischen Recherche sammeln. Hieraus können weitere Projekte entstehen. Durch die Arbeit mit ausgewählten Schicksalen werden den Jugendlichen die Mechanismen der Ausgrenzung und Verfolgung während des Nationalsozialismus vermittelt. Dabei lernen sie anhand konkreter Beispiele, wie durch eine Radikalisierung der Diskriminierung der Weg zum systematischen Massenmord geebnet wurde. Zudem kann ihnen die Vielfältigkeit der Opfergruppen bewusst gemacht werden, denn die Stolpersteine erinnern an alle während des Nationalsozialismus verfolgten Gruppen: an Jüdinnen und Juden, an Sinti und Roma, an politische und religiöse Gegner\*innen, an die Opfer der „Euthanasie“-Morde, an Homosexuelle, Zeug\*innen Jehovas und an die als „asozial“ stigmatisierten und verfolgten Menschen.

Eine intensivere Form der pädagogischen Arbeit stellt die selbständige Initiierung einer Stolperstein-Verlegung dar. Hierfür sollte ein Arbeitszeitraum von etwa sechs Monaten eingeplant werden. Die Jugendlichen lernen nicht nur die historische Recherche, sondern auch die organisatorischen Aufgaben rund um diese Form des Erinnerns kennen. Dazu zählen unter anderem das Sammeln von Spenden, die Koordination mit örtlichen Institutionen, die Kommunikation über Lokalgeschichte sowie die Konzeption und Durchführung einer Gedenkveranstaltung.

Katja Demnig steht bei der Umsetzung pädagogischer Programme als Ansprechpartnerin der Stolperstein-Stiftung zur Verfügung. Sie stellt unter anderem den Kontakt zu durchgeführten Projekten her und informiert über passendes Begleitmaterial. Sie ist erreichbar unter <paedagogik@stolpersteine.eu>.
Auch die Berliner Koordinierungsstelle Stolpersteine bietet fachliche Beratung und Unterstützung für pädagogische Stolperstein-Projekte an, allerdings nur in Berlin. Hier kann gegen eine Gebühr von acht Euro Begleitmaterial für diese Projekte bestellt werden. Darin finden sich Anregungen, die sich auch auf Projekte in Brandenburg übertragen lassen.
