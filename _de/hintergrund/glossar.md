---
title: Glossar
category: 3 hintergrund
order: 40
---

# Glossar

## A

### Aktion 14 f 13

Im April 1941 wurde im KZ Sachsenhausen die „Aktion 14 f 13“ nach dem Vorbild der sogenannten Krankenmorde in Gang gesetzt. Ziel war die Ermordung der körperlich und seelisch kranken, arbeitsunfähigen Häftlinge und anderer unerwünschter Gefangener. T4-Gutachter bestimmten die Todeskandidaten, ermordet wurden die Selektierten in den Tötungsanstalten Sonnenstein, Bernburg und Hartheim. Bis zum Ende der Mordaktion im Frühjahr 1943 wurden über 10 000 Menschen in den Tötungsanstalten ermordet.

Siehe auch: Aktion T4

### Aktion Arbeitsscheu Reich

Die Aktion „Arbeitsscheu Reich“ leitete den Beginn der staatlichen Verfolgung von Menschen ein, die von der nationalsozialistischen Rassenpolitik als „asozial“ eingestuft wurden. Gestapo und Kriminalpolizei verhafteten bei dieser Aktion zwischen dem 13. und 18. Juni 1938 im Rahmen der „vorbeugenden Verbrechensbekämpfung“ über 10 000 „Asoziale“ und deportierten sie in Konzentrationslager. Dort wurden sie mit einem schwarzen Dreieck gekennzeichnet. Nach einer Durchführungsverordnung von 1938 galt als asozial, „wer durch gemeinschaftswidriges, wenn auch nicht verbrecherisches Verhalten zeigt, dass er sich nicht in die Gemeinschaft einfügen will“. Namentlich wurden „Landstreicher, Bettler, Zigeuner, Prostituierte, Trunksüchtige sowie mit ansteckenden Krankheiten, insbesondere Geschlechtskrankheiten, behaftete Personen“ aufgeführt.

### Aktion Gewitter

Nach dem gescheiterten Attentat auf Adolf Hitler vom 20. Juli 1944 erhielt Heinrich Himmler am 14. August 1944 den Auftrag zur Verhaftung von ehemaligen KPD- und SPD-Politikern, um jede mögliche Neubildung einer Opposition im Kern zu verhindern. Mit einem Schreiben Himmlers vom 17. August 1944 an alle Gestapoleitstellen wurde die Aktion in Gang gesetzt. In ein Konzentrationslager einzuweisen waren demnach alle politischen Funktionsträger der beiden Parteien aus der Weimarer Zeit. Unter dem Decknamen Aktion „Gewitter“ – in manchen Quellen auch „Gitter“ – wurden in den folgenden Tagen über 5000 Männer und Frauen festgenommen. Bis Mitte September 1944 kamen etwa 80 Prozent der Verhafteten wieder frei.

Synonyme: Aktion Gitter

### Aktion Reinhardt

Die „Aktion Reinhardt“ begann Mitte 1942 und hatte die Tötung der Juden zum Ziel, die in den Ghettos im besetzten Polen lebten und für die deutsche Rüstungsindustrie Zwangsarbeit leisten mussten. Im Rahmen dieser Aktion wurde die Mehrheit der jüdischen Ghettobewohner in die Vernichtungslager Bełżec, Sobibór und Treblinka deportiert und dort ermordet. Die Aktion zielte auf eine möglichst vollständige Ermordung der Juden aus dem Generalgouvernement. Insgesamt ließ Odilo Globocnik im Auftrag von Heinrich Himmler im Rahmen dieser Aktion 2 Millionen Juden und etwa 50.000 Sinti und Roma ermorden.

### Aktion T4

Unter der Tarnbezeichnung „Aktion T4“ (benannt nach dem Sitz der zuständigen Dienststelle in der Berliner Tiergartenstraße 4) wurden zwischen Anfang 1940 und August 1941 70 000 kranke Erwachsene in den Gaskammern der sechs zu zentralen Tötungsanstalten umfunktionierten „Heil- und Pflegeanstalten“ in Grafeneck, Brandenburg, Hartheim, Pirna, Bernburg und Hadamar umgebracht. Organisationsform und technische Ausführung der Aktion standen Modell für die „Endlösung der Judenfrage“. Nach Protesten von kirchlicher Seite hatte Adolf Hitler 1941 die „Aktion T4“ formal eingestellt, dennoch fanden weiterhin Mordaktionen noch bis Kriegsende vor allem in Osteuropa statt.

Synonyme: T4 Aktion T4-Aktion Aktion-T4

### Arisierung

Im weiteren Sinne bezeichnete der Begriff die Verdrängung der Juden aus ihren Berufen und dem öffentlichen Leben. In erster Linie waren Juden im Sinne der „Nürnberger Gesetze“ Opfer dieses Verdrängungs- und Ausgrenzungsprozesses und es waren die als „Arier“ betrachteten deutschen Staatsbürger, die davon profitierten. Dieser Prozess begann mit der Machtübernahme 1933 und verschärfte sich im Frühjahr 1938, als Juden zunächst gezwungen wurden, alle Vermögenswerte über 5000 Reichsmark anzumelden sowie ihr Gewerbe und ihre Betriebe registrieren zu lassen. Weitere Maßnahmen zu ihrer vollständigen Verdrängung aus der deutschen Wirtschaft folgten nach der Pogromnacht vom 9. auf den 10. November 1938. Die damaligen Machthaber zwangen die als Juden kategorisierten Menschen, ihre Geschäfte, Firmen, Immobilien, Grundstücke sowie andere Wertgegenstände weit unter dem tatsächlichen Wert zu verkaufen. Ab Januar 1939 wurden sämtliche Betriebe jüdischer Eigentümer zwangsgeschlossen und Juden die Ausübung praktisch aller Berufe verboten. Die Arisierung erhöhte für Juden den ohnehin bestehenden Druck zur Auswanderung und nahm ihnen gleichzeitig die finanziellen Mittel dazu.

### Auswanderung

Zwischen 1933 und 1941 flohen mehr als die Hälfte der etwa 500 000 deutschen Juden, tausende Aktivisten der organisierten Arbeiterbewegung sowie politisch exponierte Künstler, Schriftsteller, Journalisten und Publizisten aus Deutschland. Die erste Fluchtwelle setzte unmittelbar nach der Machtübergabe an die Nationalsozialisten im Januar 1933 und dem bereits im April 1933 erlassenen „Gesetz zur Wiederherstellung des Berufsbeamtentums“ ein. Die „Nürnberger Gesetze“ vom September 1935 führten zu einer zweiten großen Auswanderungsbewegung und der Pogrom vom 9./10. November 1938 löste schließlich die dritte Fluchtwelle aus. Ab 1938 erschwerten zahlreiche Gesetze die Auswanderung. Vor der Emigration war die sogenannte „Reichsfluchtsteuer“ zu entrichten, beim Verlassen Deutschlands durften die Menschen nur 10 Reichsmark Bargeld mit sich führen. Ein weiteres immenses Hindernis waren die bald fast weltweit geltenden rigiden Einwanderungsbestimmungen. Ab 1939 waren es nur noch einige wenige südamerikanische Staaten sowie Shanghai, die eine Einreise ohne Visum erlaubten. Mit Beginn der Deportationen aus Deutschland im Oktober 1941 verbot das Regime Juden eine Auswanderung generell.

Synonyme: Emigration Flucht Exil

## C

### Columbia-Haus

Ab Sommer 1933 nutzte die Gestapo die leerstehende Berliner Militär-Arrestanstalt auf dem Tempelhofer Feld als Gefängnis für ihr Hauptquartier in der Prinz-Albrecht-Straße. Im Dezember 1934 wurde das „Columbia-Haus“ der SS unterstellt und erhielt offiziell die Bezeichnung „Konzentrationslager Columbia“. Es bestand bis 1936. Die Haftbedingungen waren unmenschlich, die hygienischen Verhältnisse, die Verpflegung und die Krankenversorgung erbärmlich. Insgesamt waren im „Columbia-Haus“ mindestens 8000 Männer und auch einige wenige Frauen eingesperrt. 1936 wurde das Haus für den Flughafenbau abgerissen.

## D

### Deportation

In der Zeit des Nationalsozialismus bezeichnete „Deportation“ die systematische Verschleppung von Millionen jüdischer Europäer, von Sinti und Roma, politisch Andersdenkenden und Widerstandskämpfern in Ghettos, Konzentrations- und Vernichtungslager. Die Deportationen wurden verharmlosend als „Evakuierung“ oder „Umsiedlung“ bezeichnet, um das eigentliche Ziel, die millionenfache Ermordung von Menschen, zu vertuschen.

### Deportationsbahnhof Anhalter Bahnhof

Ab Juni 1942 erfolgten Deportationen von Juden aus Berlin auch vom Personenbahnhof Anhalter Bahnhof. Dies waren die „Alterstransporte“, die Menschen ins Ghetto Theresienstadt brachten. Die Transporte fanden in der Regel morgens mit den planmäßigen Zügen statt, an die entsprechend weitere Wagen der 3. Klasse angehängt wurden. Mindestens 9 000 Menschen sind von diesem Bahnhof aus deportiert worden.

### Deportationsbahnhof Berlin-Grunewald

Am 18. Oktober 1941 verließ der erste Deportationszug mit Berliner Juden den Bahnhof Grunewald. Bis April 1942 wurden die Berliner Juden und Jüdinnen hauptsächlich in die Ghettos Litzmannstadt, Riga und Warschau deportiert. Ab Ende 1942 hatten die Deportationszüge das Vernichtungslager Auschwitz-Birkenau oder das Ghetto Theresienstadt zum Ziel. Mindestens 17 000 Menschen sind von diesem Bahnhof aus deportiert worden.

### Deportationsbahnhof Putlitzbrücke

Von dem sich in der Nähe befindlichen „Sammellager“ Levetzowstraße wurden Juden anfangs auf LKWs zum Bahnhof Putlitzbrücke gebracht, ab 1943 mussten sie auch zu Fuß dorthin gehen. Von einem Nebengleis des Güterbahnhofs wurden sie in das Vernichtungslager Auschwitz-Birkenau deportiert. Mindestens 30 000 Menschen sind von diesem Bahnhof aus deportiert worden.

Synonyme: Güterbahnhof Moabit

### Durchgangslager Bentschen

Im Rahmen der sogenannten Polenaktion vom 28. und 29. Oktober 1938 wies die nationalsozialistische Regierung 17 000 Menschen mit polnischem Pass aus. Für ca. 4800 von ihnen ließ sich der Ort Bentschen (Zbąszyń), der durch die weiteren Ereignisse schon seinerzeit große Aufmerksamkeit in der Presse erregte, als Grenzübergang nachweisen. Zeitzeugen sprachen von chaotischen Zuständen. Mehrere tausend Menschen irrten im Niemandsland umher, drängten sich auf dem Bahngelände, hausten im Stationsgebäude oder auf nahe gelegenen Plätzen in der polnischen Grenzstadt Bentschen sowie auf den die Stadt umgebenden Wiesen. Das Lager wurde Ende November 1938 aufgelöst. Hilfskomitees und Privatpersonen waren den Flüchtlingen dabei behilflich andere Unterkünfte zu finden.

### Durchgangslager Drancy

Das 1941 errichtete „Sammellager“ Drancy im Nordosten von Paris war Durchgangsstation für die Deportation von Juden aus Frankreich. Vor dem Zweiten Weltkrieg hatte der Gebäudekomplex zunächst als Wohnsiedlung, dann als Kaserne gedient, bevor er zum „Sammellager“ für die Deportierten wurde. Bis zu 4500 Menschen wurden hier untergebracht, zwischen dem 21. August 1941 und dem 17. August 1944 durchliefen über 70 000 Menschen dieses Lager. Sie wurden von dort mit der Bahn in die Vernichtungslager, insbesondere nach Auschwitz-Birkenau, deportiert.

### Durchgangslager Westerbork

In der Nähe von Westerbork, einem Dorf in der ostniederländischen Provinz Drenthe, befand sich ein von der niederländischen Regierung 1939 errichtetes Lager. Es diente der Unterbringung von jüdischen Flüchtlingen, die sich teils legal, teils illegal in den Niederlanden aufhielten. Die Einrichtung dieses Lagers war 1938 beschlossen worden, als sich nach der Pogromnacht die Zahl der in die Niederlande fliehenden Juden stark erhöhte. Nach dem Einmarsch deutscher Truppen in die Niederlande im Mai 1940 kam das Lager unter deutsche Verwaltung und diente weiterhin der konzentrierten Unterbringung von jüdischen Flüchtlingen aus Deutschland und Österreich. 1942 wurde aus dem Wohnlager ein „Durchgangslager“ für niederländische Juden, von dem aus die Transporte in die Vernichtungslager gingen. Von Juli 1942 bis September 1944 brachten 98 Züge ca. 96 000 Menschen nach Auschwitz, Sobibór und Bergen-Belsen; 4466 Menschen wurden (zunächst) nach Theresienstadt deportiert. Insgesamt belief sich die Zahl der über Westerbork deportierten Juden auf etwa 107 000, von denen nur etwa 5000 überlebten.

## E

### Euthanasie

„Euthanasie“ (griechisch für „schöner Tod“ oder „guter Tod“) meinte ursprünglich die bewusste Herbeiführung des Todes oder die Erleichterung des Sterbens durch Betäubungsmittel. Vom nationalsozialistischen Regime wurde das Wort als irreführende und verharmlosende Bezeichnung für die systematische Tötung physisch eingeschränkter und psychisch kranker Menschen verwendet. In unterschiedlichen Mordaktionen starben zwischen 1939 und 1945 über 300 000 Menschen im Deutschen Reich und im besetzten Europa.

## F

### Fabrikaktion

Als „Fabrikaktion“ wird heute die Verhaftung der bis dahin von der Deportation verschont gebliebenen Juden bezeichnet, die bis zum 27. Februar 1943 noch in Rüstungsbetrieben oder bei Jüdischen Kultusvereinigungen zwangsbeschäftigt waren. Bereits am 26. November 1942 hatte der Generalbevollmächtigte für den Arbeitseinsatz Fritz Sauckel den Landesarbeitsämtern mitgeteilt, dass die jüdischen Zwangsarbeiter zu „evakuieren“ seien. In Berlin sollten die jüdischen Zwangsarbeiter durch polnische ersetzt werden. Hier dauerte die Großrazzia, bei der etwa 8000 Berliner Jüdinnen und Juden, meist an ihren Arbeitsplätzen, verhaftet wurden, rund eine Woche. In fünf Transporten zwischen dem 1. und dem 6. März 1943 wurden die Gefangenen nach Auschwitz deportiert.

Synonyme: Fabrik-Aktion

## G

### Geltungsjude

Als „Geltungsjuden“ definierten die Nationalsozialisten in der ersten Verordnung zum Reichsbürgergesetz vom 14. November 1935 Menschen mit zwei „der Rasse nach volljüdischen Großelternteilen“ (sogenannte Halbjuden), die der jüdischen Religionsgemeinschaft angehörten und/oder mit einem Juden verheiratet waren. Daneben gab es die sogenannten „jüdischen Mischlinge“, die „nicht zum Judentum tendierten“ und in einer Ehe mit einem „arischen“ Partner lebten.

Synonyme: Halbjude Halbjüdin Mischling

### Gesetz zur Verhütung erbkranken Nachwuchses

Bereits am 1. Januar 1934 trat das „Gesetz zur Verhütung erbkranken Nachwuchses“ in Kraft, auf dessen Grundlage etwa 400 000 Menschen zwangsweise sterilisiert wurden, ca. 5000 starben an den Folgen des Eingriffs. Eine deutliche Radikalisierung fand schließlich mit dem sogenannten Euthanasiebefehl statt, von Adolf Hitler Ende Oktober 1939 unterschrieben und auf den 1. September rückdatiert, der die Ermordung Kranker und Behinderter zum Ziel hatte.

Synonyme: GezVeN

### Gesetz zur Wiederherstellung des Berufsbeamtentums

Dieses Gesetz, erlassen am 7. April 1933, ermöglichte es der nationalsozialistischen Regierung, Kritiker des Regimes und jüdische bzw. „nichtarische“ Staatsdiener zu entlassen oder in den vorzeitigen Ruhestand zu versetzen. Jeder Beamte musste nun nachweisen, dass er keine jüdischen Verwandten hatte. Sukzessive fand das Gesetz auch bei Angestellten und Arbeitern im öffentlichen Dienst sowie in halböffentlichen Unternehmen Anwendung.

### Gestapo

Die Geheime Staatspolizei (Gestapo) entstand 1933 als politische Polizei des nationalsozialistischen Regimes nach der rechtlichen und organisatorischen Umformung der politischen Polizeiorgane der Weimarer Republik. Zur Aufdeckung und Verfolgung aller Handlungen, die das Regime als politische Vergehen oder Verbrechen definierte, konnte die Gestapo als „vorbeugende Maßnahme“ gegen tatsächliche oder angebliche Gegner eine „Schutzhaft“ in Gefängnissen und Konzentrationslagern verhängen, Gefangene foltern und hinrichten.

### Ghetto

Die nationalsozialistischen Besatzer errichteten Ghettos als Orte der Demütigung, Ausgrenzung und Ausbeutung der jüdischen Bevölkerung. Durch die Konzentrierung der jüdischen Bevölkerung in größeren Städten machten die Besatzer die Ghettos zu Sammel- und Zwischenstationen im Vorfeld weiterer Deportationen, häufig in Vernichtungslager. Ab 1940 wurden die Ghettos von der Außenwelt abgegrenzt. Besonders in Polen und in der Sowjetunion war die Errichtung von Ghettos fester Bestandteil Judenverfolgung. Derzeit ist die Existenz von etwa 1200 Ghettos in Ostmittel- und Osteuropa belegt.

### Ghetto Kaunas

Unmittelbar nach der Besetzung der Stadt Kowno im Juni 1941 kam es zu organisierten Pogromen. Im Juli 1941 informierte die Sicherheitspolizei Vertreter der Jüdischen Gemeinde, dass alle Juden bis Mitte August 1941 in ein Ghetto ziehen müssten. Im abgeriegelten kleinen und großen Ghetto lebten etwa 30 000 Menschen. Die Mehrzahl der Juden musste Zwangsarbeit für deutsche Rüstungsunternehmen leisten. Im Herbst 1941 ermordeten die deutschen Besatzer in mehreren „Aktionen“ über 10 000 Ghettobewohner. Zwei Jahre später, 1943, wurde das Ghetto in ein Konzentrationslager umgewandelt und die SS übernahm die Kontrolle über das Ghettoleben und den Zwangsarbeitseinsatz. Im März 1944 wurden in großen Deportationen Kinder und ältere Ghettobewohner in die Vernichtungslager Auschwitz und Majdanek gebracht. Im Juli 1944 wurden etwa 8000 Ghettobewohner in weiter westlich gelegene Konzentrationslager deportiert. Vor Ankunft der Roten Armee am 1. August 1944 sind alle Gebäude des ehemaligen Ghettos gesprengt und niedergebrannt worden.

Siehe auch: Ghetto

### Ghetto Lemberg

Die deutschen Besatzungsbehörden besetzten Lwów am 30. Juni 1941; zeitgleich begann die Wehrmacht, unterstützt von der nichtjüdischen Bevölkerung, mit der Ermordung von Juden. Im November 1941 gaben die Besatzungsbehörden bekannt, dass alle Juden bis zum Mitte Dezember 1941 in ein zu errichtendes Ghetto ziehen müssten. Ende des Jahres 1941 lebten zwischen 90 000 und 110 000 Menschen im Ghetto. Im September 1942 wurden etwa 80 000 Menschen im Ghetto von der übrigen Stadt abgeriegelt. Im Vorfeld hatten große Razzien und anschließende Deportationen ins Vernichtungslager Bełżec stattgefunden. Weitere Deportationen fanden im November 1942 sowie im März 1943 statt. In Folge wurde das Ghettogelände drastisch reduziert und erhielt die offizielle Bezeichnung „Julag Lemberg“. Am Juni 1943 wurde das Lager geräumt. 7000 Menschen wurden deportiert und 3000 ermordet. Die Rote Armee befreite am 26. Juli 1944 Lemberg, zu diesem Zeitpunkt lebten nur noch 300 Juden in der Stadt.

Siehe auch: Ghetto

### Ghetto Litzmannstadt

Im jüdischen Armenviertel Bałuty der Stadt Łódź errichteten nationalsozialistische Besatzungsbehörden bis zum April 1940 ein vier Quadratkilometer großes Ghetto für ca. 160 000 Juden. Ende 1941 trafen in 20 Großtransporten etwa 20 000 Juden aus Deutschland, Österreich und Tschechien ein. In 96 Arbeitsstätten mussten die Ghettobewohner unter unwürdigsten Bedingungen leben und arbeiten. Der Massenmord an den Ghettobewohnern begann im Januar 1942 mit den Deportationen in das Vernichtungslager Kulmhof/Chełmno. Im Zuge der Auflösung des Ghettos im August 1944 kamen Juden ins Lager Auschwitz, wo die meisten in den dortigen Gaskammern ermordet wurden.

Siehe auch: Ghetto

### Ghetto Minsk

Unmittelbar nach dem Überfall auf die Sowjetunion errichteten die Besatzer im Juli 1941 das etwa zwei Quadratkilometer große Ghetto Minsk für ca. 100 000 einheimische Juden. Bereits im November 1941 wurden auch Juden aus dem Deutschen Reich ins Ghetto Minsk deportiert. Diese waren in einem „Sonderghetto“ untergebracht, ohne nennenswerten Kontakt zum „Hauptghetto“. Fast alle Menschen, die unter unwürdigen und erbärmlichsten Bedingungen im Ghetto überlebt hatten, wurden entweder bei einem Massaker im Mai 1943 oder bei der Auflösung des Ghettos im September 1943 erschossen.

Siehe auch: Ghetto

### Ghetto Riga

Dieses Ghetto entstand kurz nach der Besetzung Lettlands im August 1941 im „Moskauer Viertel“ der Stadt. Auf etwa 9000 Quadratmetern sollten 30 000 einheimische Juden von der übrigen Bevölkerung separiert werden. Im Vorfeld der ersten Transporte aus dem Deutschen Reich wurden Ende November 1941 27 500 Ghettobewohner erschossen. Von November 1941 bis Februar 1942 kamen 20 Transporte mit deutschen, österreichischen und tschechischen Juden ins Ghetto Riga. Im Sommer 1943 entstand in der Nähe des Ghettos das Konzentrationslager Riga-Kaiserwald, in das über 400 Juden aus dem Ghetto verschickt wurden. Im November 1943 wurden Kranken und Kinder in großer Zahl ins KZ Auschwitz deportiert und das Ghetto damit so gut wie „aufgelöst“.

Siehe auch: Ghetto

### Ghetto Theresienstadt

Theresienstadt (Terezín) war eine Festungsstadt in der Nähe von Prag. Die 3700 nichtjüdischen Einwohner wurden 1940 von den Nationalsozialisten aus der Stadt evakuiert, um dort ein Ghetto einzurichten. Ins Ghetto Theresienstadt kamen vor allem ältere jüdische Deutsche, die im Ersten Weltkrieg ausgezeichnet worden waren, jüdische Partner aus nicht mehr bestehenden „Mischehen“ und „Geltungsjuden“. Sie mussten „Heimeinkaufsverträge“ abschließen, in denen die Deportierten ihre Vermögenswerte gegen „Betreuung und Pflege“ in Theresienstadt abtraten. Die Nationalsozialisten missbrauchten die kulturellen Aktivitäten im Ghetto für ihre Propaganda und täuschten komfortable Lebensbedingungen vor. In Theresienstadt starben etwa 33 500 Menschen. Unzählige deportierten die Besatzer in Vernichtungslager und nur etwa 23 000 konnte die Rote Armee am 8. Mai 1945 befreien.

Siehe auch: Ghetto

### Ghetto Wilna

Die deutsche Wehrmacht besetzte Vilnius am 24. Juni 1941, bereits Anfang September wurde ein Ghetto errichtet. Etwa 30 000 Juden wurden ins „große“ Ghetto und ca. 11 000 ins „kleine“ Ghetto eingewiesen. In den folgenden Monaten terrorisierten die Besatzungsorgane die Ghettobewohner immer wieder mit Erschießungsaktionen vor allem von nicht mehr arbeitsfähigen Menschen. Die Räumung des Ghettos begann im August 1943 mit Deportationen nach Lettland und Estland. Am 23. September 1943 wurde das Ghetto gänzlich geräumt. Die Stadt Vilnius wurde am 13. Juli 1944 von der Roten Armee befreit. Zu diesem Zeitpunkt lebten von ehemals 75 000 Juden nur noch etwa 3000.

Siehe auch: Ghetto


## H

### Heimeinkaufsvertrag

Deutsche Juden, die in das „Altersghetto“ Theresienstadt deportiert werden sollten, schlossen auf Veranlassung der Gestapo mit der Reichsvereinigung der Juden in Deutschland „Heimeinkaufsverträge“ ab. Darin wurde den älteren Juden die lebenslange kostenfreie Unterbringung, ihre Verpflegung und Krankenversorgung zugesagt. Neben einer errechneten Vorauszahlung wurden weitere Abgaben, Spenden und Vermögensübertragungen gefordert. Tatsächlich fanden die Deportierten in Theresienstadt überfüllte und kaum geheizte Wohnstätten, mangelhafte Ernährung und unzureichende ärztliche Versorgung vor. Die Vermögenswerte fielen später dem Reichssicherheitshauptamt (RSHA) zu.

Siehe auch: Ghetto Theresienstadt

### Holocaust

Das Wort „Holocaust“ kommt aus dem Griechischen und bezeichnet ein religiöses Brandopfer. Seit der US-amerikanischen Fernsehserie „Holocaust“ aus dem Jahre 1979 ist der Begriff weit verbreitet und steht für die Ermordung von sechs Millionen Juden während der Zeit des Nationalsozialismus. Da die ursprüngliche Bedeutung des Wortes „Holocaust“ die Assoziation einer Selbstopferung der Juden zulässt, während sie in Wirklichkeit ermordet wurden, benutzen Juden selbst in der Regel das neu-hebräische Wort „Shoa“, das Sturm, plötzlichen Untergang und Verderben bedeutet.

Synonyme: Shoa


## J

### Jiddisch

Jiddisch ist eine von den aschkenasischen Juden aus dem Mittelhochdeutschen hervorgegangene westgermanische, mit hebräischen, aramäischen, romanischen, slawischen und weiteren Sprachelementen angereicherte Sprache.

### Jom HaShoah ve-HaGwurah

Bedeutet wörtlich „Tag des Gedenkens an die Shoah und Heldentum“.

### Judenhäuser

Mit dem Reichsgesetz über die Mietverhältnisse von Ende April 1939 schränkte die nationalsozialistische Regierung den Mieterschutz sowie die freie Wohnungswahl für Juden erheblich ein. Den Wohnungsbehörden ermöglichte dieses Gesetz, Juden in bestimmten Häusern zu konzentrieren. Häuser jüdischer Eigentümer, in denen ab 1939 ausschließlich jüdische Mieter zwangsweise wohnen mussten, bezeichnete die damalige Terminologie als „Judenhäuser“. Eine direkte Beschlagnahme von Häusern zu diesem Zweck gab es nicht.

Synonyme: Judenhaus

### Judenreferat der Gestapo

Das „Judenreferat“ war eine Gestapo-Abteilung im Reichssicherheitshauptamt (RSHA) während des Zweiten Weltkrieges. Hier wurde ab 1941 die „Endlösung der Judenfrage“ administrativ koordiniert und organisiert. Die Mitarbeiter des „Judenreferates“ waren somit maßgeblich am Holocaust beteiligt. Leiter dieser Dienststelle war ab Dezember 1939 durchgehend Adolf Eichmann; ab 1941 war Rolf Günther sein ständiger Stellvertreter.

### Judenstern

Eine Polizeiverordnung vom September 1941 über die Kennzeichnung von Juden zwang alle im Deutschen Reich und im Protektorat Böhmen und Mähren von den Nationalsozialisten als jüdisch eingestuften Menschen ab dem sechsten Lebensjahr, sich einen gelben sechszackigen „Judenstern“ an ihre Kleidungsstücke zu heften. Ohne diesen durften sie sich nicht mehr in der Öffentlichkeit zeigen. Der Stern musste zudem mit dem Wort „Jude“ beschriftet und auf der linken Brustseite festgenäht sein.


## K

### Kindereuthanasie

Unter dem Deckmantel von Pädagogik, Krankenpflege, Medizin und Wissenschaft planten die Nationalsozialisten eine Geheimaktion zur Tötung behinderter Kinder. Die „Kindereuthanasie“ begann 1939 und dauerte bis Kriegsende an. Initialzündung war der Fall eines schwer behinderten Jungen, der an die Kanzlei des Führers herangetragen wurde. Der Begleitarzt Hitlers, Dr. Karl Brandt, veranlasste die Ermordung des fünf Monate alten Kindes. Hitler befahl anschließend, dass in vergleichbaren Fällen ebenso zu verfahren sei und beauftragte die Kanzlei des Führers mit der Durchführung der „Kindereuthanasie“.

Siehe auch: Euthanasie

### Kindertransporte

Unter dem Eindruck der Novemberpogrome im nationalsozialistischen Deutschland übten Hilfsorganisationen Druck auf die englische Regierung aus, doch zumindest Minderjährigen die Einreise zu erlauben. Kein anderer europäischer Staat war in einem solchen Umfang wie Großbritannien bereit, jüdische Kinder aus Deutschland, Österreich, der Tschechoslowakei sowie der Freien Stadt Danzig aufzunehmen. Gleichwohl sollten etwaige innenpolitische Spannungen weitestgehend vermieden werden. Daher mussten sämtliche anfallende Kosten von privaten Spendern übernommen werden. Mit Kriegsausbruch 1939 endeten die Kindertransporte, bis dahin waren etwa 10 000 Kinder in England aufgenommen worden. Dennoch lagen zu diesem Zeitpunkt der Reichsvertretung der Juden in Deutschland noch über 10 000 Anträge vor.

### Kommunistische Partei Deutschlands

Die Kommunistische Partei Deutschlands wurde am 30. Dezember 1918 gegründet. Sie erstrebte die sofortige Regierungsübernahme, da sie sich als Vertreter der Arbeiter, also der Mehrheit der Bevölkerung, ansah. Daher lehnte sie es ab, an den Wahlen zur Nationalversammlung teilzunehmen. Nach der Ermordung von Karl Liebknecht und Rosa Luxemburg durch rechtsradikale Offiziere 1919 änderte die KPD ihre Haltung und beteiligte sich 1920 an den Reichstagswahlen. Als sich die USPD im Oktober 1920 spaltete und ihr linker Flügel zur KPD übertrat, wurde die KPD zur Massenpartei. Mit der Weltwirtschaftskrise ab 1929 wurde sie zur Partei der Arbeitslosen. Sie bekämpfte neben den rechten und konservativen Parteien auch die SPD („Sozialfaschisten“), die im Reich, in Preußen und in Berlin für Kürzungen der Sozialausgaben mitverantwortlich war.

Siehe auch: Revolutionäre Gewerkschafts-Opposition Rote Frauen- und Mädchenbund Rote Frontkämpferbund

Synonyme: KPD

### Konzentrationslager

Die von der SS geführten Konzentrationslager dienten der Internierung politischer Gegner und unerwünschter Minderheiten. Während des Zweiten Weltkriegs wurden sie immer mehr Sammelort für billige Arbeitskräfte, die vor allem in der Kriegsindustrie eingesetzt wurden. Die Häftlinge lebten unter menschenunwürdigen Bedingungen, sie schliefen auf einfachen Holzpritschen, bekamen wenig Essen und mussten schwer arbeiten. Tausende starben an Hunger, Erschöpfung, Krankheiten oder an den Folgen von Misshandlungen. Einige Konzentrationslager in den besetzten Gebieten in Osteuropa waren gleichzeitig Vernichtungslager. Dort wurde ab 1941 die „fabrikmäßige“ Ermordung von Juden, Sinti und Roma, Kriegsgefangenen und politischen Gegnern betrieben.

### KZ Auschwitz

Das KZ Auschwitz, auch als Auschwitz I bezeichnet, wurde am 14. Juni 1940 in der preußischen Provinz Oberschlesien bei der polnischen Stadt Oświęcim auf einem ehemaligen österreichisch-ungarischen Kasernengelände eingerichtet, zunächst als Haftstätte für polnische Oppositioneller und Intellektuelle. Seit März 1942 waren auch Frauen im Stammlager Auschwitz inhaftiert, für die bis Juli 1942 formal jedoch das KZ Ravensbrück zuständig war. Die Häftlinge kamen seit 1943 aus allen besetzten Ländern Europas; die Mehrzahl waren Juden. Die Lebensbedingungen waren von Beginn an mörderisch. Zehntausende starben an den Folgen körperlicher Schwerstarbeit beim Ausbau des Lagers, völlig unzureichender Versorgung, durch medizinische Versuche oder wurden gezielt getötet. Seit Oktober 1941 mordete die SS in Auschwitz I erstmals mit Zyklon B. Die Rote Armee befreite Auschwitz am 27. Januar 1945. Insgesamt starben im Konzentrationslager Auschwitz und im Vernichtungslager Auschwitz-Birkenau mindestens 1,1 Million Menschen.

Siehe auch: Konzentrationslager

Synonyme: KZ-Auschwitz KZ Auschwitz I KZ-Auschwitz I Auschwitz I

### KZ Bergen-Belsen

Bergen-Belsen war ein nationalsozialistisches Konzentrationslager bei Hannover. Es wurde im Frühjahr 1941 von der Wehrmacht errichtet und diente zunächst als Lager für Kriegsgefangene. Bis Februar 1942 starben dort mindestens 18 000 sowjetische Kriegsgefangene. Von April 1943 an wurde Bergen-Belsen als Konzentrationslager für jüdische Gefangene genutzt. Ab März 1944 entwickelte sich Bergen-Belsen zu einem „Aufnahmelager“ für Häftlinge aus anderen Konzentrationslagern, die zumeist auf sogenannten Todesmärschen aus den frontnahen Konzentrationslagern dorthin getrieben worden war. Ohne ärztliche Behandlung, ausreichende Nahrung und Unterkünfte überließ die Konzentrationslager-SS die Häftlinge ihrem Schicksal. Etwa 50 000 Häftlinge und 20 000 sowjetische Kriegsgefangene kamen im Lager ums Leben. Rund 14 000 Überlebende starben bis Ende Juni 1945 an den Folgen der Haftbedingungen.

Siehe auch: Konzentrationslager

Synonyme: KZ-Bergen-Belsen Konzentrationslager Bergen-Belsen Lager Bergen Belsen

### KZ Buchenwald

Das Konzentrationslager Buchenwald in der Nähe von Weimar entstand 1937 auf dem Ettersberg. Die ersten Insassen waren politische Gegner des NS-Regimes, Zeugen Jehovas, Homosexuelle, Sinti und Roma sowie Strafgefangene. Ende 1938 wurden auch Tausende von Juden in Buchenwald inhaftiert. Ab Oktober 1942 wurden die meisten jüdischen Häftlinge nach Auschwitz deportiert und über 8000 sowjetische Kriegsgefangene von der SS ermordet. Nach Auflösung der Lager im Osten überführte die SS Anfang 1945 Tausende der Insassen nach Buchenwald, das zu Jahresbeginn mit über 100 000 Häftlingen das größte noch bestehende Konzentrationslager war. Ab dem 6. April 1945 begann die Lagerleitung, die jüdischen Häftlinge auf „Todesmärsche“ zu schicken und verließ am 11. April das Lager. Am selben Tag trafen amerikanische Truppen im Lager ein. Im KZ Buchenwald waren insgesamt über 240 000 Menschen aus allen europäischen Ländern inhaftiert, von denen mindestens 50 000 starben.

Synonyme: KZ-Buchenwald Konzentrationslager Buchenwald

### KZ Dachau

Am 22. März 1933 wurde in der Nähe von Dachau bei München ein Konzentrationslager für männliche Häftlinge errichtet. Die ersten Häftlinge waren politische Gegner des NS-Regimes: Kommunisten, Sozialdemokraten, Gewerkschafter, teilweise auch liberale und konservative Politiker. Später folgten „Kriminelle“, Zeugen Jehovas, engagierte Christen, Sinti und Roma, Homosexuelle sowie vor allem Juden. Nach Zerschlagung der „Rest-Tschechei“ im März 1939 und nach Beginn des Zweiten Weltkriegs im September 1939 wurden vor allem ausländische Häftlinge nach Dachau transportiert. Im Winter 1942 begannen SS-Ärzte in Dachau mit medizinischen Experimenten an Häftlingen. Alle jüdischen Häftlinge des Lagers wurden ab dem 5. Oktober 1942 nach Auschwitz deportiert. Um die Befreiung der Häftlinge durch anrückende alliierte Truppen zu verhindern, schickte die Lagerverwaltung am 26. April 1945 rund 7000 Häftlinge auf einen „Todesmarsch“ in Richtung Süden. Am 29. April 1945 wurde Dachau von amerikanischen Einheiten befreit. Zwischen 1933 und 1945 waren in Dachau über 200 000 Menschen inhaftiert, mindestens 30 000 der registrierten Häftlinge kamen dort ums Leben.

Siehe auch: Konzentrationslager

Synonyme: KZ-Dachau Konzentrationslager Dachau

### KZ Flossenbürg

Das Konzentrationslager Flossenbürg bestand von 1938 bis 1945 in der Gemeinde Flossenbürg bei Weiden im Oberpfälzer Wald. Das KZ war von Anfang an als ein Konzentrationslager zur Ausbeutung von Zwangsarbeitern, ein Lager zur „Vernichtung durch Arbeit“ geplant. In diesem ersten Lager der „zweiten Generation“ von Konzentrationslagern richtete sich der Terror nicht mehr nur gegen die politischen Gegner der Nazis, vielmehr sollten gesellschaftliche Außenseiter durch brutale Zwangsarbeit entweder „brauchbare Glieder der Volksgemeinschaft“, das heißt, willfährige Helfer werden, oder der „Vernichtung durch Arbeit“ zum Opfer fallen. Von 1938 bis zum April 1945 waren mindestens 85 000 Menschen im KZ Flossenbürg inhaftiert, mindestens 30 000 Häftlinge starben.

Synonyme: KZ-Flossenbürg Konzentrationslager Flossenbürg

### KZ Mauthausen

Das Konzentrationslager Mauthausen war das größte Konzentrationslager in Österreich. Es befand sich 20 Kilometer östlich von Linz und bestand ab dem 8. August 1938. Noch kurz vor der Befreiung wurden im Konzentrationslager Häftlinge ermordet. Die genaue Zahl aller Getöteten ist nicht bekannt. Jedoch geht man von mindestens 100 000 Todesopfern aus. Im April 1945 hatte die SS damit begonnen, alle Akten zu vernichten, die auf ihre Verbrechen im Lager hinwiesen. Darunter fiel auch das Abmontieren der Gaskammer, die 1941 im Keller des Krankenbaus eingerichtet worden war. Danach flohen die SS-Männer und die Häftlinge wurden vom Volkssturm und von der Wiener Feuerwehr bewacht. Am 5. Mai 1945 wurde das Lager durch die vorrückenden Truppen der 11. US-Panzerdivision der 3. US-Armee befreit.

Siehe auch: Konzentrationslager

Synonyme: KZ-Mauthausen Konzentrationslager Mauthausen

### KZ Neuengamme

1938 zunächst als Außenlager des KZ Sachenhausen gegründet, wurde Neuengamme im Frühjahr 1940 ein eigenständiges KZ. Im Verlauf des Krieges kamen mehr als 100 000 Menschen aus allen besetzten Ländern Europas als KZ-Häftlinge nach Neuengamme. Insgesamt waren nach gegenwärtigen Erkenntnissen über 80 000 Männer und mehr als 13 000 Frauen mit einer Häftlingsnummer registriert. Das KZ diente zudem der Staatspolizeileitstelle Hamburg als Hinrichtungsstätte, etwa 1400 Personen wurden dort exekutiert. 1942 wurden 448 russische Kriegsgefangene mit Zyklon B vergast. Mehrfach wurden im KZ medizinische Experimente an Häftlingen durchgeführt. Insgesamt kamen mindestens 42 900 Menschen im Stammlager Neuengamme, in den Außenlagern oder im Zuge der Lagerräumungen ums Leben. Zusätzlich sind mehrere tausend Häftlinge nach ihrem Abtransport aus dem KZ Neuengamme in andere Konzentrationslager oder nach Kriegsende an den Folgen der KZ-Haft gestorben.

Siehe auch: Konzentrationslager

Synonyme: KZ-Neuengamme Konzentrationslager Neuengamme

### KZ Ravensbrück

Das Konzentrationslager Ravensbrück war das einzige „Schutzhaftlager“ für Frauen auf reichsdeutschem Gebiet. Es wurde 1938 errichtet und lag auf dem Gebiet des heutigen Landes Brandenburg. In den Jahren 1939 bis 1945 waren etwa 132 000 Frauen und Kinder, 20 000 Männer und 1000 weibliche Jugendliche als Häftlinge registriert. Sie wurden zu Bauarbeiten und in der Kriegsproduktion eingesetzt. Siemens & Halske errichtete neben dem KZ-Gelände 20 Werkhallen. Es gab mehr als 70 Nebenlager, die über das gesamte Reich verteilt waren. Ende 1944 richtete die SS in einer Baracke neben dem Krematorium eine provisorische Gaskammer ein. Hier wurden von Ende Januar bis April 1945 ca. 5000 bis 6000 Häftlinge vergast. Am 30. April 1945 erlebten die ca. 3000 verbliebenen Häftlinge ihre Befreiung durch die sowjetische Armee.

Siehe auch: Konzentrationslager

Synonyme: KZ-Ravensbrück Konzentrationslager Ravensbrück

### KZ Riga-Kaiserwald

Im lettischen Kaiserwald, einem Vorort Rigas, ließ die SS seit 15. März 1943 von 500 politischen und als „kriminell“ eingestuften Häftlingen aus Sachsenhausen ein Konzentrationslager errichten. Ab Sommer 1943 diente Kaiserwald als Haftstätte für Juden und Jüdinnen, überwiegend aus den aufgelösten Ghettos im Baltikum, und glich auch in seinen Funktionen eher den Ghettos als den im Reich betriebenen KZ. Das Stammlager war die Schaltzentrale für 16 in der Umgebung eingerichtete Außenlager, in denen die Häftlinge einen Truppenübungsplatz für die SS errichteten sowie Zwangsarbeit in Wirtschafts- und Bekleidungslagern der Wehrmacht leisteten. Insgesamt waren während des Krieges 16 000 bis 19 000 Juden im KZ-Komplex Riga inhaftiert. Weil die SS vor ihrem Rückzug die Spuren des Mordes an den baltischen Juden beseitigen wollte, mussten Häftlinge seit Anfang 1944 Leichen aus den Massengräbern um Riga ausgraben und verbrennen. Bevor die SS am 6. August 1944 mit der Evakuierung des Lagers begann, hatte sie zahlreiche Kinder, Kranke, Ältere und nicht Transportfähige erschossen. Die Überlebenden wurden in drei Transporten per Schiff in das KZ Stutthof bei Danzig überstellt. Die Rote Armee befreite Riga am 13. Oktober 1944 und richtete auf dem Gelände des verlassenen KZ ein Kriegsgefangenenlager ein.

Synonyme: KZ Kaiserwalde

### KZ Sachsenhausen

Im August 1936 wurde das Konzentrationslager Sachsenhausen rund 35 Kilometer nordöstlich von Berlin bei Oranienburg gebaut. Zunächst wurden in Sachsenhausen politische Gegner inhaftiert. Später folgten Juden, Homosexuelle, Sinti und Roma, Zeugen Jehovas und Kriminelle. Tausende Häftlinge starben an Unterernährung, Krankheit, Erschöpfung und Misshandlungen oder wurden von der SS ermordet. Zehntausende Häftlinge wurden zur Zwangsarbeit herangezogen. Ab Oktober 1941 begannen Massenerschießungen, denen über 12 000 sowjetische Kriegsgefangene zum Opfer fielen. Als sich die Rote Armee im Vormarsch auf das Lager befand, wurden über 33 000 Häftlinge in Richtung Ostsee getrieben. Bei diesem „Todesmarsch“ starben ca. 6000 Gefangene. Etwa 3000 im Lager verbliebene Häftlinge wurden am 22. April 1945 von polnischen und sowjetischen Einheiten befreit. Zwischen 1936 und 1945 waren mehr als 200 000 Menschen in Sachsenhausen inhaftiert, von denen mehrere Zehntausend die Haft nicht überlebten.

Siehe auch: Konzentrationslager

Synonyme: KZ-Sachsenhausen Konzentrationslager Sachsenhausen

### KZ Sonnenburg

Das Konzentrationslager Sonnenburg bei Küstrin (Kostrzyn nad Odrą) bestand vom 3. April 1933 bis zum 23. April 1934. Unterstellt war das KZ dem Berliner Polizeipräsidenten. Die Mehrheit der Gefangenen waren Funktionäre der verbotenen KPD. Insgesamt waren im KZ Sonnenburg etwa 1000 Menschen eingesperrt. Der Lageralltag war von einer besonderen Brutalität gegenüber den Gefangenen geprägt. Ab dem Frühjahr 1934 wurde das Gebäude erneut zu einem Zuchthaus, in dem neben Straftätern auch Zwangsarbeiter und politische Gefangene inhaftiert waren. Bevor die Wachleute vor der herannahenden Roten Armee Richtung Westen flohen, erschossen sie in der Nacht vom 30. auf den 31. Januar 1945 fast alle Häftlinge.

Siehe auch: Konzentrationslager

Synonyme: KZ-Sonnenburg Konzentrationslager Sonnenburg

### Köpenicker Blutwoche

Die Köpenicker Blutwoche kann als ein Höhepunkt des SA-Terrors nach der Ernennung Adolf Hitlers zum Reichskanzler betrachtet werden. Das reichsweite Verbot des Deutschnationalen Kampfrings zum Anlass nehmend, begannen Mitglieder des SA-Sturmbanns 15 am Vormittag des 21. Juni 1933 gezielt, politische Gegner und Juden in Berlin-Köpenick zu verhaften. Diese Maßnahmen eskalierten in den folgenden Tagen und waren von einer unglaublichen Brutalität geprägt. Hunderte von Regimegegnern wurden in der Woche vom 21. Juni 1933 von der SA auf das Schwerste misshandelt, mindestens 22 starben an den Folgen.

## M

### Menschenversuche

An der mörderischen Rassen- und Bevölkerungspolitik des Nationalsozialismus wirkten von Anfang an zahlreiche deutsche Ärzte und Wissenschaftler mit. Viele von ihnen begeisterten sich für biologistische Rassentheorien und rassistische Vererbungslehre und betrieben Forschungen zu deren „wissenschaftlichen“ Bestätigung. In den Konzentrationslagern führten sie „Selektionen“ und verbrecherische Menschenversuche durch. In manchen Konzentrationslagern bedienten sich skrupellose Ärzte der Häftlinge als „Versuchskaninchen“ für entsetzliche medizinische Experimente. Beinahe ausnahmslos war der wissenschaftliche Nutzen dieser Versuche ohne jeden Wert. Die Mehrheit der Opfer starb dabei oder wurde anschließend ermordet, um ihre Körper zu analysieren oder sie als Zeugen der Medizinverbrechen zu beseitigen. Andere blieben dauerhaft krank oder für immer verkrüppelt. Wer zunächst überlebte, aber nicht wieder zu relativer Gesundheit und zu ausreichenden Kräften kam, wurde zumeist bei einer der zahlreichen „Selektionen“ für den Tod in den Gaskammern ausgesondert.

## N

### Nationalsozialistische Deutsche Arbeiterpartei

Die Nationalsozialistische Deutsche Arbeiterpartei wurde 1919 in München gegründet. Adolf Hitler stieg erst zum Führer der Partei und dann 1933 zum deutschen Reichskanzler auf. Die NSDAP erstrebte nicht die Rückkehr zur Monarchie, sondern wollte ein „germanisches Führertum mit Gefolgschaft“. Im Gegensatz zu den anderen Parteien der Rechten wollte sie zunächst nicht die Arbeiterbewegung bekämpfen, sondern ihr mit einem nationalen Sozialismus die Wähler wegnehmen. Der Antisemitismus war von Anfang an ein wichtiger Bestandteil des Parteiprogramms. Nachdem spätestens 1934 der Führerstaat eingerichtet worden war, verlor die Partei an Bedeutung. Allerdings blieb die NSDAP-Mitgliedschaft für viele Berufskarrieren unabdingbar.

Synonyme: NSDAP

### Nicht privilegierte Mischehe

Ab Anfang der 1930er-Jahre lebten ca. 35 000 Juden in „Mischehen“. Bis 1938 waren Juden, die in solchen Ehen lebten, ebenso von allen Ausgrenzungsmaßnahmen betroffen wie Paare, in denen beide Juden waren. Doch im Dezember 1938 schuf Adolf Hitler die Kategorien der „privilegierten“ und der „nicht-privilegierten“ Mischehen. War der Mann Jude und die Ehe kinderlos, galt diese Ehe für Nationalsozialisten als „nicht-privilegiert“. Diese Kategorie galt auch, wenn ein Partner jüdisch war und die Kinder jüdisch erzogen worden waren oder falls der nichtjüdische Ehepartner mit der Eheschließung zum Judentum konvertiert war. Im Falle einer geplanten Auswanderung wurden diese Paare wie Juden behandelt. Der jüdische Partner war zwar verpflichtet, den Judenstern zu tragen, er/sie war jedoch zunächst von Deportationen zurückgestellt. Im Falle einer Scheidung oder beim Tod des nichtjüdischen Ehepartners drohte dem jüdischen Partner die Deportation, meist nach Theresienstadt.

Siehe auch: Geltungsjude Privilegierte Mischehe

Synonyme: nicht-privilegierte Mischehe

### Novemberpogrome 1938

In der Nacht vom 9. zum 10. November 1938 fanden in ganz Deutschland die bis dahin schwersten öffentlichen Ausschreitungen gegen Juden und ihre Einrichtungen in der Neuzeit statt. Der Begriff „Reichskristallnacht“ wurde von der damaligen Bevölkerung in Anspielung auf die bei den Ausschreitungen zerstörten Schaufensterscheiben jüdischer Läden geprägt. In der Presse wurden die Vorfälle heruntergespielt, in der NS-internen Aktensprache ist von der „Judenaktion“ oder der „Novemberaktion“ die Rede. Angehörige von SA, SS und Hitlerjugend und Sympathisanten steckten Synagogen in Brand und plünderten etwa 7000 Geschäfte jüdischer Einzelhändler. Fast 100 Menschen wurden ermordet, Tausende jüdische Männer in Konzentrationslager verschleppt.

Synonyme: Reichsprogromnacht Reichspogrome Novemberpogrom Novemberpogromnacht „Reichskristallnacht“

### Nürnberger Gesetze

Die „Nürnberger Gesetze“ wurden am 15. September 1935 auf dem Parteitag der NSDAP in Nürnberg beschlossen. Das „Reichsbürgergesetz“ sprach nur dem „Reichsbürger“ die vollen politischen Rechte zu; er wurde definiert als Staatsbürger „deutschen oder artverwandten Blutes“. Das „Gesetz zum Schutze des deutschen Blutes und der deutschen Ehre“ (kurz: „Blutschutzgesetz“) enthielt den neuen Straftatbestand „Rassenschande“, der Eheschließungen und Geschlechtsverkehr zwischen Juden und „Deutschblütigen“ unter Strafe stellte. Zwei Monate später erging die „Erste Verordnung zum Reichsbürgergesetz“, in der ausdrücklich festgelegt wurde, dass Juden keine „Reichsbürger“ sein können.

Siehe auch: Rassenschande

Synonyme: Nürnberger Rassengesetze Nürnberger Rassegesetze

## P

### Polenaktion 28./29.10.1938

Nach dem Anschluss Österreichs ans Deutsche Reich und der Besetzung des Sudetenlandes dachten viele in Deutschland und Österreich lebende polnische Staatsbürger an eine Rückkehr. Eine solche war jedoch der polnischen Regierung nicht willkommen. Daher verabschiedete das polnische Parlament im März 1938 ein Gesetz, wodurch allen länger als 5 Jahre im Ausland lebenden Polen die Staatsbürgerschaft entzogen werden würde. Damit ihre Dokumente Gültigkeit behielten, brauchten diese Polen einen Kontrollvermerk des zuständigen Konsulats. Einer solchen Regelung kam die nationalsozialistische Regierung Ende Oktober zuvor, indem sie Polen einen Ausweisungsbefehl erteilte, sie verhaftete und gegen ihren Willen nach Polen abschob. Die Spuren der meisten ausgewiesenen polnischen Juden verlieren sich in den folgenden Jahren in den unzähligen Ghettos, wohin sie meist gemeinsam mit ihren Angehörigen deportiert wurden.

Synonyme: Polenaktion

### Privilegierte Mischehe

Ab Anfang der 1930er-Jahre lebten ca. 35 000 Juden in „Mischehen“. Bis 1938 waren Juden, die in solchen Ehen lebten, ebenso von allen Ausgrenzungsmaßnahmen betroffen wie Paare, in denen beide Juden waren. Doch im Dezember 1938 schuf Adolf Hitler die Kategorien der „privilegierten“ und der „nicht-privilegierten“ Mischehen. Als „privilegiert“ wurden Paare betrachtet, bei denen die Frau jüdisch im „rassischen“ Sinne war. Dies jedoch nur, wenn sie keine oder nichtjüdisch erzogene Kinder hatten. Privilegiert waren Paare auch dann, wenn der Mann im „rassischen“ Sinne Jude war, die Kinder jedoch nichtjüdisch erzogen waren. So kategorisierte Familien durften in ihren Wohnungen bleiben, das Vermögen konnte auf den nichtjüdischen Partner oder die Kinder übertragen werden. Auch einen Judenstern mussten die jüdischen Ehepartner nicht tragen. Bis 1945 waren sie zudem durch die Ehe vor der Deportation geschützt.

Siehe auch: Nicht privilegierte Mischehe

## R

### Rassenschande

Das „Gesetz zum Schutze des deutschen Blutes und der deutschen Ehre“ vom 15. September 1935 verbot Juden, „Mischehen“ einzugehen, und stellte außerehelichen Geschlechtsverkehr unter Gefängnis- bzw. Zuchthausstrafe. Das Gesetz führte zu zahllosen Denunziationen und zu tausenden Verfahren. Zwischen 1935 und 1945 wurden ca. 2000 jüdische und nichtjüdische Männer verurteilt. Bei Juden führte eine Verurteilung zu „Schutzhaft“ und Deportation. Eine nicht bekannte Anzahl jüdischer Frauen wurden ohne Gerichtsverfahren in Konzentrationslager eingeliefert, später erfolgte zudem die Überstellung in Vernichtungslager.

Siehe auch: Nürnberger Gesetze

### Reichsbanner Schwarz-Rot-Gold

Das „Reichsbanner Schwarz-Rot-Gold – Bund deutscher Kriegsteilnehmer und Republikaner“ wurde 1924 in Magdeburg als ein überparteiliches Bündnis zum Schutz der Weimarer Republik gegen ihre Feinde gegründet. Allerdings überwog der Anteil der Sozialdemokraten in der Mitgliedschaft deutlich, Schätzungen gehen von bis zu 90 Prozent aus. Das Reichsbanner war ein Veteranenverband, in dem Kriegsteilnehmer des Ersten Weltkrieges offensiv für die Republik eintraten. Im Januar 1932 schloss sich das Reichsbanner mit den freien Gewerkschaften und anderen Verbänden zur Eisernen Front zusammen.

### Reichsfluchtsteuer

Die „Reichsfluchtsteuer“ wurde durch die „Vierte Verordnung des Reichspräsidenten zur Sicherung von Wirtschaft und Finanzen und zum Schutz des inneren Friedens“ im Dezember 1931 von der Regierung Brüning eingeführt. Sie sollte in der Situation der verschärften Weltwirtschaftskrise Kapitalflucht, also den plötzlichen Transfer von Vermögen, Geld, Edelmetallen oder Sachwerten ins Ausland, verhindern. Nach ihrem Machtantritt 1933 instrumentalisierten die Nationalsozialisten die „Reichsfluchtsteuer“ für ihre judenfeindliche Politik. Zur Emigration gezwungene Juden mussten ab 1938 die Hälfte ihres Privatvermögens an den Staat abtreten. Dazu kamen noch die Kosten der Auswanderung wie Passgebühren u.ä., „Vorzeigegelder“ sowie eine 1939 eingeführte „Auswanderungsabgabe“.

Siehe auch: Auswanderung

### Reichsvereinigung der Juden in Deutschland

Die „Reichsvertretung der Deutschen Juden“ wurde 1933 als Interessenverband jüdischer Organisationen und Gemeinden gegründet, mit Leo Baeck als Präsidenten. Bis 1939 musste die Organisation sich mehrfach umbenennen und umstrukturieren. Am 4. Juni 1939 wurden schließlich alle jüdischen Verbände und jüdischen Gemeinden aufgrund der 10. Verordnung zum Reichsbürgergesetz (1939) zwangsweise in die RVJD überführt. Sie war dem Reichssicherheitshauptamt direkt unterstellt, die regionalen Zweigstellen unterstanden den jeweiligen Gestapostellen. Alle Personen, die nach den „Nürnberger Gesetzen“ als Juden galten, mussten ihr beitreten und Pflichtbeiträge entrichten. Hauptaufgabe der RVJD war zunächst die Förderung der Auswanderung, hinzu kamen die Organisation des Schulwesens sowie Maßnahmen zur Berufsausbildung und -umschichtung. Mit der wachsenden Verelendung der Juden, insbesondere älterer Menschen, trat die Fürsorge in den Vordergrund der Arbeit, u.a. wurden Heime und Suppenküchen eingerichtet. Die RVJD führte eine Zentralkartei aller Juden, derer sich u.a. die Gestapostellen bedienten. Auf Anweisung und nach Vorgaben waren Deportationslisten aufzustellen. Andererseits war die RVJD Arbeitgeber für über 6000 Juden, die dadurch der Zwangsarbeit entgingen, ein Einkommen besaßen und zunächst vor der Deportation geschützt waren.

Siehe auch: Nürnberger Gesetze

Synonyme: RVJD

### Revolutionäre Gewerkschafts-Opposition

Die Revolutionäre Gewerkschafts-Opposition entstand nach einem auf Drängen der Sowjetunion 1929 zustande gekommenen Beschluss der KPD, bei Betriebsratswahlen grundsätzlich mit eigenen Listen anzutreten. Die RGO entwickelte sich so zu einer kommunistischen Sondergewerkschaft, was zum Ausschluss zahlreicher Kommunisten aus dem Allgemeinen Deutschen Gewerkschaftsbund führte. Im März 1932 hatte die RGO rund 200 000 Mitglieder. Im selben Jahr erregte sie in ganz Deutschland Aufmerksamkeit, als sie zusammen mit den Nationalsozialisten einen Arbeitskampf mit den Berliner Verkehrsbetrieben führte („BVG-Streik“). 1933 wurde die RGO verboten.

Siehe auch: Kommunistische Partei Deutschlands Rote Frauen- und Mädchenbund Rote Frontkämpferbund

Synonyme: RGO

### Rote Frauen- und Mädchenbund

Der Rote Frauen- und Mädchenbund wurde 1925 als überparteilich konzipierte Massenorganisation von Kommunistinnen gegründet. Seine Arbeit galt nicht nur der politischen Agitation wie dem Kampf gegen den Abtreibungsparagrafen 218 und für die Emanzipation der Frauen, sondern konzentrierte sich hauptsächlich auf die Bekämpfung der Not sozial benachteiligter Bevölkerungsgruppen.

Siehe auch: Kommunistische Partei Deutschlands Revolutionäre Gewerkschafts-Opposition Rote Frontkämpferbund

Synonyme: RFMB

### Rote Frontkämpferbund

Der RFB wurde im Juli 1924 auf Initiative der KPD in Halle/Saale als Wehr- und Schutzorganisation gegründet. Der RFB war propagandistisch tätig und schützte kommunistische Veranstaltungen. Die Mitglieder waren uniformiert und traten oft in militärisch formierten Marschkolonnen in Erscheinung. 1929 wurde der RFB verboten, war aber bis 1933 weiter aktiv.

Siehe auch: Kommunistische Partei Deutschlands Revolutionäre Gewerkschafts-Opposition Rote Frauen- und Mädchenbund

Synonyme: RFB


## S

### SA-Gefängnis Papestraße

Auf dem weitläufigen Kasernengelände an der General-Pape-Straße befand sich von März bis Dezember 1933 ein Gefängnis der „SA-Feldpolizei“, einer Sondereinheit der SA. Im Keller des SA-Gefängnisses „Papestraße“, das offiziell als „Schutzhaftlager“ diente, wurden mehr als 2000 Menschen inhaftiert, mehrheitlich politische Gegner des Nationalsozialismus. Viele Häftlinge wurden misshandelt und mindestens 20 von ihnen ermordet.

### Sammellager in Berlin

Vor einer Deportation in Ghettos und Vernichtungslager zwangen die Gestapo sowie die Stapoleitstelle in Berlin Juden in „Sammellager“. Kontrolliert von der Gestapo, erfolgten dort die organisatorischen Vorbereitungen für den Transport sowie der Einzug des Vermögens der zu Deportierenden. Vor den Augen der Bevölkerung, organisatorisch unterstützt durch private Spediteure, wurden diese von der Gestapo zu den Bahnhöfen gebracht. In Berlin gab es während des Zweiten Weltkrieges über die gesamte Stadt verteilt insgesamt 15 „Sammellager“.

Synonyme: Sammellager

### Schabbat

Schabbat ist der wöchentliche Feier- und Ruhetag des Judentums, der von Freitag- bis Samstagabend dauert.

Synonyme: Schabbes

### Schutzhaft

Das Instrument der „Schutzhaft“ wurde vom NS-Regime dazu eingesetzt, politisch, „rassisch“ oder sozial missliebige Personen willkürlich festzunehmen und in Gefangenen- und Konzentrationslager zu deportieren. Als rechtliche Grundlage der „Schutzhaft“ wurde die – nach dem Reichstagsbrand erlassene – „Verordnung des Reichpräsidenten zum Schutz von Volk und Staat“ vom 28. Februar 1933 angesehen. Die „Schutzhaft“ unterlag keiner richterlichen Überprüfung, ihre Opfer hatten kein Recht auf anwaltlichen Beistand. Häufig diente sie – etwa nach einem Freispruch, der Entlassung eines Angeschuldigten aus der Untersuchungshaft oder im Anschluss an die Verbüßung einer Freiheitsstrafe – der Korrektur gerichtlicher Entscheidungen, die dem Regime missliebig waren.

Siehe auch: Konzentrationslager

### Schutzstaffel

Die Schutzstaffel war eine paramilitärische Formation der NSDAP. Während der Weimarer Republik noch eine Saalschutztruppe, entwickelte sie sich nach der Machtübergabe an die Nationalsozialisten zum Macht- und Terrorinstrument. Ihr unterstand das System der Konzentrationslager. Die Organisation und Durchführung des Genozids an den europäischen Juden sowie den Sinti und Roma lag ebenso in ihrer Verantwortung. Militärisch war sie durch eigene Verbände (der Waffen-SS) sowie als Teil der Besatzungsmacht an den Verbrechen während des Zweiten Weltkriegs beteiligt.

Siehe auch: Nationalsozialistische Deutsche Arbeiterpartei

Synonyme: SS

### Shoah

Shoah ist die hebräische Bezeichnung für Holocaust.

Siehe auch: Holocaust

### Sicherheitsdienst

Der Sicherheitsdienst der SS wurde 1931 als Geheimdienst der NSDAP gegründet. Durch ihn sollten Nachrichten über politische Gegner und parteiinterne Vorgänge beschafft werden. 1934 wurde der SD zum alleinigen Nachrichtendienst der NSDAP erklärt, 1937 eine exakte Aufgabenteilung zwischen SD und Gestapo vorgenommen. Zu den Aufgaben des SD gehörte es später auch, Lageberichte über die Stimmung in der Bevölkerung („Meldungen aus dem Reich“) zu schreiben, die politische Zuverlässigkeit bestimmter Personen zu überprüfen und Pläne für die Ausbeutung der annektierten Gebiete zu machen. Der Sicherheitsdienst verfügte 1944 über 6500 hauptamtliche Mitarbeiter und 30 000 Spitzel.

Siehe auch: Nationalsozialistische Deutsche Arbeiterpartei

Synonyme: SD

### Sicherheitspolizei

Am 17. Juni 1936 wurde der „Reichsführer SS“, Heinrich Himmler, zum „Chef der Deutschen Polizei im Reichsministerium des Innern“ ernannt. Himmler hatte damit ein wesentliches Ziel auf dem Weg zum „SS-Staat“ erreicht, die Zentralisierung der deutschen Polizei und ihre weitere Verschmelzung mit dem Apparat der SS. Himmler ordnete die Polizei neu, indem er sie in „Ordnungspolizei“ und „Sicherheitspolizei“ aufteilte. Zum „Chef der Sicherheitspolizei“ ernannte er den SS-Gruppenführer Reinhard Heydrich, der gleichzeitig Chef des Sicherheitsdienstes (SD) der SS war. Die Sipo bestand aus der Kriminalpolizei, der Grenzpolizei und der Geheimen Staatspolizei.

Siehe auch: Nationalsozialistische Deutsche Arbeiterpartei

Synonyme: Sipo

### Sozialdemokratische Partei Deutschland

Die SPD hat kein exaktes Entstehungsdatum. Sie selbst beruft sich auf die Gründung des Allgemeinen Deutschen Arbeitervereins (ADAV) durch Ferdinand Lassalle am 23. Mai 1863 in Leipzig. Nach dem Außerkrafttreten des Gesetzes gegen die gemeingefährlichen Bestrebungen der Sozialdemokratie im Herbst 1890 änderte die Partei ihren Namen in Sozialdemokratische Partei Deutschlands. In der jungen Weimarer Republik stellte die SPD bis 1925 mit Friedrich Ebert den Reichspräsidenten und war in einigen Reichsregierungen vertreten. Ihre soziale Basis während der Weimarer Republik bildeten vor allem die gewerkschaftlich organisierten Facharbeiter. Die Nein-Stimmen der SPD bei der Abstimmung über das nationalsozialistische Ermächtigungsgesetz wahrten die Ehre der demokratischen Parteien, während alle bürgerlichen Parteien diesem Gesetz zustimmten. Am 21. Juni 1933 wurde gegen die SPD ein Betätigungsverbot erlassen, am 14. Juli 1933 wurde die Partei verboten.

Synonyme: SPD

### Strafgefängnis Plötzensee

Mit der Machtübernahme der Nationalsozialisten verschärften sich die Haftbedingungen im Strafgefängnis Plötzensee. Die Haftanstalt diente zudem als Untersuchungsgefängnis für politische Gefangene. Im Strafgefängnis Plötzensee wurden während der nationalsozialistischen Herrschaft 2891 Todesurteile vollstreckt.

Synonyme: Gefängnis Plötzensee

### Sturmabteilung

Die SA war aus dem 1920 gegründeten „Ordnungsdienst“ der NSDAP hervorgegangen. Sie war – nach außen hin – für den Schutz von Veranstaltungen zuständig, nach dem Willen des Parteiführers Hitler für gewaltsame Auseinandersetzungen mit dem politischen Gegner, die in aller Regel von der SA provoziert wurden. Die von ihr gegen Ende der 1920er Jahre in allen großstädtischen Arbeitervierteln eingerichteten „Sturmlokale“ waren die Basis für den nationalsozialistischen Straßenterror. Für die NSDAP war die SA in dieser „Kampfzeit“ das Instrument, mit dem sie die Dominanz von SPD und KPD in der Arbeiterschaft zu brechen suchte.

Siehe auch: Nationalsozialistische Deutsche Arbeiterpartei

Synonyme: SA

## T

### Tötungsanstalt Bernburg

Die Tötungsanstalt Bernburg befand sich in einem abgetrennten Teil der „Landes-Heil- und Pflegeanstalt“ in Bernburg an der Saale (Sachsen-Anhalt) und war zwischen dem 21. November 1940 und dem 30. Juli 1943 eine „Euthanasie-Anstalt“ der „Aktion T4“. Hier wurden 9384 Kranke und Behinderte aus 33 Fürsorge- und Pflegeeinrichtungen sowie rund 5000 Häftlinge aus sechs Konzentrationslagern mit Kohlenstoffmonoxid in einer Gaskammer ermordet.

Siehe auch: Aktion T4 Euthanasie

Synonyme: Landes-Heil- und Pflegeanstalt Bernburg

### Tötungsanstalt Brandenburg

Die Euthanasie-Tötungsanstalt Brandenburg an der Havel befand sich im Alten Zuchthaus in der Neuendorfer Straße 90c. In den Gebäuden war zunächst von August 1933 bis Februar 1934 das KZ Brandenburg untergebracht. Bereits im Januar 1940 war in Brandenburg die Tötung von Menschen durch Kohlenstoffmonoxid erprobt worden. Ab Februar 1940 wurde die angebliche „Heilanstalt Brandenburg“ zur „Euthanasie-Anstalt“, in der bis zum Oktober 1940 mehr als 9000 psychisch Kranke und geistig Behinderte aus Nord- und Mitteldeutschland in der Gaskammer ermordet wurden.

Siehe auch: Euthanasie

Synonyme: Heilanstalt Brandenburg

### Tötungsanstalt Grafeneck

In der Tötungsanstalt Grafeneck bei Gomadingen (Landkreis Reutlingen, Baden-Württemberg) wurden während der nationalsozialistischen Krankenmorde, der sogenannten Aktion T4, vom nationalsozialistischen Regime 1940 systematisch 10 654 behinderte Menschen, vor allem aus Bayern, Baden und Württemberg, ermordet.

Siehe auch: Euthanasie

### Tötungsanstalt Hadamar

Ende 1940 wurde die Landesheilanstalt Hadamar bei Limburg umgebaut, um sie als Tötungsanstalt für die „Aktion T4“ einzusetzen. Eine Gaskammer, ein Sezierraum und zwei Verbrennungsöfen wurden installiert, außerdem wurde eine Busgarage errichtet. Zwischen Januar und August 1941 starben in dieser Gaskammer 10 122 Menschen. In einer zweiten Mordphase übernahm die Anstalt erneut die Funktion einer Tötungsanstalt. Von August 1942 bis zum 26. März 1945 starben weitere 4411 Menschen.

Siehe auch: Euthanasie

Synonyme: Landesheilanstalt Hadamar

### Tötungsanstalt Hartheim

Im März 1940 begannen im Schloss Hartheim in Oberösterreich die Umbauarbeiten zu einer Tötungsanstalt für die „Ostmark“, Bayern und die Untersteiermark (Teilgebiete des heutigen Slowenien). Innerhalb weniger Wochen wurden die notwendigen Adaptierungsarbeiten durchgeführt und eine Gaskammer sowie ein Krematorium eingebaut. Anfang Mai 1940 traf der erste Transport in Hartheim ein. Bis zum Euthanasiestopp im August 1941 wurden in der Tötungsanstalt mehr als 18 000 Menschen ermordet. In Hartheim ging auch nach der Einstellung der „Aktion T4“ das Morden weiter. Die nächsten Opfer waren KZ-Häftlinge aus Mauthausen im Rahmen der „Aktion 14f13“, möglicherweise wurden kurz vor dem Ende des NS-Regimes auch noch „OstarbeiterInnen“ ermordet. Im Herbst 1944 wurde die Tötungsanstalt Hartheim aufgelöst und mit der Beseitigung aller belastenden Spuren begonnen.

Siehe auch: Aktion 14 f 13 Aktion T4 Euthanasie

Synonyme: Schloss Hartheim

### Tötungsanstalt Meseritz/Obrawalde

Die 1904 als vierte „Irrenanstalt“ der Provinz Posen im heutigen Polen eröffnete Einrichtung wurde nach Beendigung der „Aktion T4“ in eine „Stätte systematischer Krankenmorde“ umorganisiert. Aus allen Teilen des Dritten Reiches (u.a. aus Berlin, Schleswig, Galkhausen, Bethel, Düsseldorf, Göttingen, Marsberg, Uchtspringe und Hamburg) wurden Patienten eingeliefert und nach ihrer Ankunft zunächst entsprechend ihrer Arbeitsfähigkeit selektiert. Ermordet wurden sie durch Injektionen oder orales Verabreichen von überdosierten Medikamenten. Bis zum Jahr 1945 starben in Meseritz/Obrawalde mindestens 18 000 Menschen.

Siehe auch: Aktion T4 Euthanasie

Synonyme: Obrawalde

### Tötungsanstalt Pirna-Sonnenstein

Die Tötungsanstalt Pirna-Sonnenstein befand sich in der ehemaligen Festung Schloss Sonnenstein bei Pirna. In diesen Räumen wurden in den Jahren 1940 und 1941 im Rahmen der „Aktion T4“ 13 720 Menschen umgebracht. Dabei handelte es sich vorwiegend um psychisch Kranke und geistig Behinderte, aber auch Häftlinge aus Konzentrationslagern.

Siehe auch: Aktion 14 f 13 Aktion T4 Euthanasie

### Tötungsanstalt Uchtspringe

Die Landesheilanstalt Uchtspringe diente seit 1940 als „Zwischenanstalt“ der „Aktion T4“ für die Tötungsanstalten Brandenburg und Bernburg. Denn die Deportationen aus den „Ursprungsanstalten“ führten in der Regel nicht direkt in eines der Tötungszentren, sondern gingen über „Zwischenanstalten“, um die Angehörigen zu täuschen und die Spur der Kranken zu verwischen. Nach dem offiziellen „Euthanasie-Stopp“ am 24. August 1941 gehörte Uchtspringe zu den Anstalten, in denen Ärzte und Pflegepersonal weiterhin töteten. Hinter der Fassade einer „normalen“ Anstaltsroutine wurden die kranken Menschen jetzt nicht mehr durch Gas, sondern mit Medikamenten, durch Morphiumspritzen, Nahrungsentzug oder Vernachlässigung ermordet.

Siehe auch: Aktion T4 Euthanasie Tötungsanstalt Bernburg Tötungsanstalt Brandenburg

Synonyme: Landesheilanstalt Uchtspringe

## V

### Verfolgung Homosexueller

Eine seit dem 19. Jahrhundert bestehende gesetzliche Verfolgung von Homosexuellen verschärften die Nationalsozialisten 1935. Ab 1937 mussten Homosexuelle damit rechnen, nach der Verbüßung ihrer Strafe zur „Umerziehung“ in ein Konzentrationslager deportiert zu werden. Wer „mehr als einen Partner verführt habe“, so eine Anordnung Himmlers 1940, sei nach der Entlassung aus dem Gefängnis grundsätzlich in „Vorbeugehaft“ zu nehmen. Parallel zu diesen Repressionsmaßnahmen wurde ab 1933 die Indikation zur zwangsweisen Kastration ausgeweitet. So konnten Verurteilte einer Haft oder einer Einweisung in ein Konzentrationslager entgehen, indem sie einer Kastration zustimmten.

Synonyme: Homosexuellen Verfolgung § 175

### Vernichtungslager

Die ersten Vernichtungslager entstanden Ende 1941, weil die Besatzungsorgane eine gezielte und konzentrierte Ermordung möglichst vieler Menschen wollten. In Chełmno-Kulm, Bełżec, Sobibór und Treblinka wurden die ankommenden Häftlinge unmittelbar in Gaskammern getötet. In den beiden größten Vernichtungslagern Auschwitz-Birkenau und Lublin-Majdanek, die an Konzentrationslager angeschlossen waren, wurde ein Großteil der Häftlinge unmittelbar nach der Ankunft ermordet. Einige mussten zunächst bis zur vollständigen körperlichen Erschöpfung arbeiten. Waren sie krank oder nicht mehr kräftig genug, kamen sie in den für die Vernichtung bestimmten Lagerbezirk.

### Vernichtungslager Auschwitz-Birkenau

Das Lager Auschwitz-Birkenau, auch Auschwitz II genannt, wurde wenige Kilometer entfernt vom Stammlager Auschwitz auf dem Areal des Dörfchens Brzezinka (Birkenau) errichtet. Die Arbeiten begannen 1941 mit Häftlingen aus dem Stammlager, ab 1942 leitete die SS Transporte direkt nach Birkenau. Seit Sommer 1942 wurden die aus nahezu ganz Europa deportierten Juden bei ihrer Ankunft an der Rampe von Birkenau nach ihrer Arbeitsfähigkeit „selektiert“. Nur wer als Häftling registriert wurde, erhielt eine Nummer; alle Übrigen wurden unmittelbar nach ihrer Ankunft in den Gaskammern erstickt. Bis Sommer 1944 ließ das RSHA 1,1 Millionen Juden nach Birkenau deportieren, von denen etwa 900.000 unmittelbar nach ihrer Ankunft ermordet wurden. Auch etwa 20.000 Sinti und Roma starben an den verheerenden Bedingungen im „Zigeunerlager“ oder wurden im Gas erstickt. Am 27. Januar 1945 befreite die Rote Armee 7000 schwerkranke Häftlinge.

Siehe auch: Vernichtungslager

Synonyme: Lager Birkenau Vernichtungslager Auschwitz Auschwitz II Vernichtungslager Birkenau

### Vernichtungslager Bełżec

Im Rahmen der sogenannten Aktion Reinhardt, deren Ziel es war, alle Juden des Generalgouvernements zu ermorden, begann die Zentralbauleitung der SS im November 1941 mit dem Bau des Lagers Bełżec in der Nähe des Ortes Bełżec. Die Entscheidung für die Errichtung von Bełżec fiel nach Einstellung des „Euthanasieprogramms“ im August 1941. Die Erfahrungen der am „Euthanasieprogramm“ beteiligten SS-Männer wurden bei der Vernichtung der Juden genutzt. Bełżec war das erste Lager, in dem die Nationalsozialisten stationäre Gaskammern errichteten. Bis Dezember 1942 ermordete die Konzentrationslager-SS etwa 500 000 Juden.

Siehe auch: Vernichtungslager

### Vernichtungslager Kulmhof

Nahe dem Dorf Chełmno, 70 km nordwestlich von Łódź, wurde im Dezember 1941 das Vernichtungslager Kulmhof errichtet, das aus einem sogenannten Schloss und einem Waldlager bestand. Es wurde vom Dezember 1941 bis März 1943 genutzt und dann erneut vom April 1944 bis Januar 1945. Im Vernichtungslager Kulmhof wurden etwa 152 000 Menschen getötet. Die Ermordeten waren primär Juden aus dem Warthegau und dem Ghetto Łódź, Roma aus dem Burgenland und russische Kriegsgefangene. Im Vernichtungslager Kulmhof machte die Konzentrationslager-SS bei der Ermordung der Menschen exzessiven Gebrauch von Gaswagen.

Siehe auch: Vernichtungslager

Synonyme: Vernichtungslager Chełmno Chełmno

### Vernichtungslager Lublin-Majdanek

Das KZ Majdanek war das erste deutsche Konzentrationslager der SS-Inspektion der Konzentrationslager (IKL) im besetzten Polen. Wie Auschwitz-Birkenau war das KZ Majdanek zeitweise auch ein Vernichtungslager. Es bestand von Oktober 1941 an, bis es am 23. Juli 1944 als erstes Lager von der Roten Armee befreit wurde. Ein Großteil der Inhaftierten waren polnische politische Gefangene und Juden, darüber hinaus war das Lager auch Sammelstelle für die deportierte Landbevölkerung aus Polen und der Sowjetunion. Nach Aufständen in den beiden Vernichtungslagern Sobibór und Treblinka erschoss die Konzentrationslager-SS in Majdanek im November 1943 innerhalb weniger Stunden 17 000 Juden. So sollten mögliche Widerstandsaktionen verhindert werden. Insgesamt sind bis zum Juli 1944 78 000 Menschen umgebracht worden, von diesen waren mindestens 60 000 Juden.

Siehe auch: Vernichtungslager

### Vernichtungslager Sobibór

Im Herbst 1941 beauftragte Heinrich Himmler den SS-und Polizeiführer des Distrikts Lublin mit der Ermordung der dort lebenden Juden. Nach dem Vorbild des bereits fertiggestellten Vernichtungslagers Bełżec begann die SS einige Monate später mit dem Bau des Todeslagers Sobibór bei Lublin. Anfang Mai 1942 erreichten die ersten Transporte mit polnischen, österreichischen und tschechischen Juden das Vernichtungslager. Im Oktober 1942 gingen in Sobibór sechs neue Gaskammern in Betrieb, in denen rund 1300 Menschen gleichzeitig umgebracht werden konnten. Im Juli 1943 ordnete Himmler die Umwandlung Sobibórs in ein Konzentrationslager an, in dem erbeutete Munition sortiert und gelagert werden sollte. Obwohl auf dem Lagergelände bereits mit ersten Bauarbeiten für die neue Funktion Sobibórs begonnen wurde, waren die Häftlinge von der baldigen Liquidierung des Lagers überzeugt. Am 14. Oktober 1943 organisierten sie einen Aufstand, in dessen Folge einigen Gefangenen die Flucht gelang. Nach dieser Revolte ermordete die SS alle im Lager befindlichen Juden und zerstörte sämtliche Gebäude.

Siehe auch: Vernichtungslager

### Vernichtungslager Treblinka

Im Sommer 1942 errichtete die SS bei Treblinka ein Vernichtungslager zur Ermordung von Juden im Rahmen der „Endlösung“. Die Massentötungen begannen am 23. Juli 1942. Den ankommenden Juden wurde erklärt, sie befänden sich in einem „Durchgangslager“ und würden nach dem Duschen in ein Arbeitslager überstellt. Nach Männern und Frauen getrennt, mussten die Deportierten sich ausziehen und ihr Gepäck abgeben. Anschließend trieb man sie in die als „Duschräume“ getarnten Gaskammern. Ein aus Juden bestehendes Arbeitskommando musste anschließend die Leichen nach versteckten Wertsachen durchsuchen und in Massengräber werfen. Bis zum Frühjahr 1943 wurden hunderttausende polnische, slowakische, griechische, mazedonische und jugoslawische Juden in Treblinka vergast. Anfang März 1943 ließ die SS die Massengräber öffnen und die Leichen verbrennen. Am 2. August 1943 gelang es einigen Häftlingen, Waffen zu erbeuten und zu fliehen. Die zurückgebliebenen Gefangenen wurden erschossen. Anschließend ließ die SS das Lager abreißen. In dem Vernichtungslager Treblinka wurden innerhalb eines Jahres über 900 000 Menschen ermordet. Nach einem Häftlingsaufstand haben rund siebzig Menschen überlebt.

Siehe auch: Vernichtungslager

### Volksgerichtshof

Der Volksgerichtshof war ein 1934 von den Nationalsozialisten neu geschaffenes Gericht, das über Hoch- und Landesverrat urteilte. Urteile des Volksgerichtshofs waren endgültig, es gab keine Berufungsinstanz. Abwandlung der Urteile war nur durch einen Gnadenerlass des „Führers und Reichskanzlers“ Adolf Hitler möglich, der die Richter persönlich ernannte. Standorte des Volksgerichtshofs in Berlin waren in der Bellevuestr. 5 und in der Elßholzstr. 31. Die Richter am Volksgerichtshof fällten über 5000 Todesurteile.


## W

### Warschauer Ghetto

Die Jüdische Gemeinde in Warschau war zum Zeitpunkt der deutschen Besatzung im September 1939 mit über 380 000 Mitgliedern die größte Europas. Am 2. Oktober 1940 erfolgte der Befehl zur Bildung eines Ghettos in einem festgelegten Teil der Altstadt, der Mitte November 1940 vom Rest der Stadt abgeriegelt wurde. Die Lebensbedingungen im überfüllten Ghetto waren katastrophal: die hygienischen Verhältnisse waren miserabel, es fehlte an Lebensmitteln und Medikamenten. Insgesamt wurden bis 1943 fast eine halbe Million Menschen ins Warschauer Ghetto verschleppt. Im April 1943 beschlossen die deutschen Behörden die Auflösung des Ghettos, jedoch stießen sie dabei am 19. April 1943 auf bewaffnete Gegenwehr der jüdischen Bevölkerung. Der aus verschiedenen jüdischen Parteien und Gruppen gebildeten jüdischen Kampforganisation ZOB (Żydowska Organizacja Bojowa) gelang es, mehrere Wochen lang Widerstand zu leisten. Erst mit der Niederbrennung des gesamten Ghettogeländes konnten die deutschen Einheiten das Gebiet bis zum 16. Mai 1943 wieder unter ihre Kontrolle bringen. Die Überlebenden des Aufstands wurden in die Vernichtungslager Treblinka und Majdanek sowie in Zwangsarbeitslager deportiert.

Siehe auch: Ghetto


## Z

### Zionismus

Zionismus bezeichnet eine politische Ideologie von Juden und die damit verbundene Bewegung, die auf die Errichtung, Rechtfertigung und Bewahrung eines jüdischen Nationalstaats in Palästina abzielt. Der Zionismus wird als Ideologie den Nationalismen und als politische Bewegung den Nationalbewegungen zugerechnet.

### Zuchthaus Brandenburg-Görden

Mit der Machtübernahme der Nationalsozialisten verschärften sich die Haftbedingungen im Zuchthaus Brandenburg drastisch. Bis Kriegsbeginn waren 60 Prozent der Inhaftierten politische Gefangene und mit Kriegsbeginn kamen viele Häftlinge aus dem besetzten Europa hinzu. Ab 1940 fungierte das Zuchthaus zudem als Hinrichtungsstätte: Vom 1. August 1940 bis 20. April 1945 wurden in Brandenburg-Görden 2030 Todesurteile vollstreckt.
