---
layout: post
title:  "Stolperstein-Tagung 2022"
date:   2022-11-21 12:00:00 +0100
categories: stolpersteine brandenburg
---

***Am 3. Dezember 2022 veranstaltet das Aktionsbündnis Brandenburg gemeinsam mit dem Landesjugendring Brandenburg die dritte Stolperstein-Tagung in Potsdam und digital.***

# Erinnerung innovativ gestalten. Tagung über Stolpersteine, lokale Geschichtsarbeit und neue Formen des Gedenkens

Wie können Erinnerungen lebendig gehalten werden? Welche neuen Wege des Erinnerns braucht es, wenn Zeitzeug_innen nicht mehr am Leben sind und ihre Geschichten nicht mehr erzählen können? Welche Chancen und Möglichkeiten bieten digitale Technologien und andere innovative Erinnerungsformate, wo liegen aber auch die Grenzen?

Mit diesen Fragen beschäftigt sich die dritte landesweite Vernetzungstagung zum Thema Stolpersteine in Brandenburg: Erinnerung innovativ gestalten ***am 3. Dezember 2022 im Haus der Natur, Lindenstraße 34, 14467 Potsdam***

Das Aktionsbündnis Brandenburg und der Landesjugendring Brandenburg wollen mit Ihnen gemeinsam einen Raum für Austausch schaffen und verschiedene Themen in Workshops bearbeiten.

Stolpersteine sind ein internationales Denkmal- und Kunstprojekt und wurden 1992 von dem Künstler Gunter Demnig initiiert. Hier in Brandenburg wurden in den letzten 30 Jahren über 1.000 dieser Steine
verlegt – durch das Engagement vieler verschiedenen Initiativen vor Ort. Wir haben die im Land verlegten Stolpersteine in einer Datenbank gesammelt und diese Daten über die Webseite Stolpersteine Brandenburg für alle zugänglich gemacht.

[Die Anmeldung zur Tagung ist möglich über dieses Formular.](https://form.jotform.com/stolpersteine/anmeldung-zur-stolperstein-tagung)

***Ablauf der Fachtagung***

- ***9.30 Uhr*** Ankommen und Anmeldung
- ***10 Uhr*** Begrüßung und fachliches Kennenlernen
- ***10.30 Uhr*** Eröffnungsrede Brigitte Faber-Schmidt, Abteilungsleiterin Ministerium für Wissenschaft, Forschung und Kultur
- ***11 Uhr*** Impulsvortrag „Plädoyer für eine digitale und innovative Erinnerungskultur“ mit Beispielen aus dem WDR-Projekt „Stolpersteine NRW“ von Stefan Domke und Elena Riedlinger, (Projektleitung)
- ***12 Uhr*** Mittagsimbiss und Gelegenheit zur Vernetzung
- ***13 Uhr*** Workshops
- ***15 Uhr*** Buchvorstellung „Steine des Anstoßes. Die Stolpersteine zwischen Akzeptanz, Transformation und Adaption“ durch die Mit-Herausgeberin Irmgard Zündorf, Leibniz-Zentrum für Zeithistorische Forschung Potsdam (ZZF)

***Workshop 1: Erinnerungen jugendgerecht gestalten***  
Gemeinsam wollen wir der Frage nachgehen, wie es gelingen kann, Erinnerungsorte und -formate zu entwickeln, die die Bedürfnisse der jungen Generation ansprechen. Wie müssen diese gestaltet sein, damit sie das Interesse von Jugendlichen wecken? Marie Lemke, 20 Jahre alt, Studentin und ausgebildete Jugendguide zur NS-Geschichte vor Ort, berichtet über ihr Engagement in Strausberg und ihre Erfahrungen in der Jugendbeteiligung. Melanie Ebell stellt die pädagogischen Ansätze und Projekte der historisch-politischen Jugendbildung der Fachstelle Zeitwerk im Landesjugendring vor.

Referentinnen:  
Marie Lemke, Jugendguide zur NS-Geschichte vor Ort  
Melanie Ebell, Landesjugendring Brandenburg e.V.

***Workshop 2: Neue Vermittlungsmethoden in der Erinnerungsarbeit***  
– Beschreibung folgt –

Referentin:  
Nicola Andersson, Cultural Heritage Expert

***Workshop 3: Möglichkeiten und Grenzen der biografischen Forschung im Brandenburgischen Landeshauptarchiv zu Opfern der nationalsozialistischen Judenverfolgung***  
Vorgestellt werden relevante Bestände für die Recherche nach Unterlagen zur Verfolgung der Jüdinnen und Juden in Brandenburg. Im Mittelpunkt stehen die Akten im Bestand Rep. 36 A Oberfinanzpräsident Berlin-Brandenburg zum Umgang mit dem Vermögen im Zuge von Deportation und Emigration.

Referent:  
Thomas Ulbrich, Landeshauptarchiv Brandenburg

***Workshop 4: Wie recherchiert man die Beteiligung von Familienmitgliedern am Nationalsozialismus?***  
Anekdoten über den Onkel bei der Wehrmacht und Sätze wie „Oma war immer gegen Hitler“ kennen wir alle. Die Zeit ist günstig, tradierten Familienlegenden auf den Grund zu gehen. Der Workshop will Menschen anregen, sich mit der NS-Vergangenheit der eigenen Familie auseinanderzusetzen.

Referentin:  
Katrin Raabe, NS-Familien-Geschichte: hinterfragen – erforschen – aufklären e.V.

[Hier gibt es das PDF des Tagungsflyer zum Download.](https://aktionsbuendnis-brandenburg.de/wp-content/uploads/2022/11/Stolperstein-Tagung_2022.pdf)



Ausgeschlossen von der Veranstaltung sind Personen, die rechtsextremen Organisationen angehören, der rechtsextremen Szene zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische, antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten sind. Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und diesen Personen den Zutritt zur Veranstaltung zu verwehren oder von dieser zu verweisen.
