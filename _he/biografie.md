---
title: כתוב ביוגרפיה
category: 2 להיות פעיל
category_de: aktiv werden
order: 30
---

# איך לכתוב ביוגרפיה?

<img src="{{ "/assets/images/steine/stolpersteine-siegfried-sara-levy-mit-blumen.jpg" | relative_url }}" alt="Gunter Demnig bei der Verlegung" class="w-25 float-left ml-3 mt-3">

במוקד היוזמה להנחת אבן נגף נמצא איתור הביוגרפיה של אדם נרדף.לעניין זה
מהותי לדעת את פרטי החיים של האדם הנרדף, תחנות הרדיפה או המנוסה ומקום
המגורים האחרון בימי הנאציזם שנבחר בחופשיות על ידי האדם הנרדף. באמצעות
הפרטים הללו אפשרית הנצחה אקטיבית: אזרחיות/ים חוקרות/ים את גורלות הנרדפות
והנרדפים ותורמות/ים בכך תרומה אקטיבית להנצחה. מטרת המחקר אינה אפוא רק
להשיג את הפרטים הנחוצים; אלא המחקר אמור לחרוג מעבר לגבולות הנחוץ, ובמהלך
העיסוק במקרה הפרטי להעלות את זכר החיים הספציפיים של האדם המדובר. התחילו
בישוב עצמו. נסו ליזום תהליך תקשורת שכונתי. גשו לדבר עם תושבות/ים בעבר
וכיום, עם אגודות מקומיות, קהילות דתיות, מפלגות, בתי ספר, מוסדות חינוך
והכשרה, בתי ספר ללימודים גבוהים ואוניברסיטאות. צרו קשר עם חוקרות/י מולדת
ותתשאלו עדות/ים מן התקופה או את צאצאיהן/ם. כדאי לברר:

-   אישים: קרובי משפחה, ידידות/ים, חברות/ים לספסל הלימודים, עמיתות/ים
    לעבודה
-   מגורים: רחוב, דירה בבניין, כתובות נוספות, שינויים בנוף העירוני
-   משפחה: לידות, הטבלות, חתונות, יובלות, מקרי מוות, שינויי שם
-   הכשרה ומקצוע: תעודות גמר בית ספריות ומקצועיות, פעילות מקצועית,
    לימודים גבוהים
-   אירועים: שינויים אחרי 1933, תגובת הישוב למדיניות הנאצית

אתן/ם עצמכן/ם אחראיות/ים לתוצאות המחקר ולטיפול בפרטים אישיים. לכן הקפידו
על כך שרשימת המקורות תהיה מלאה, בידקו את הנתונים שנמסרו על ידי אחרים
והפנו במקרה הצורך לעבודות האלה. עליכן/ם גם להקפיד לפרסם רק מקורות
ומסמכים מקוריים שבנוגע אליהם ניתנה לכן/ם רשות מטעם הארכיונים או בני
המשפחה.

להלן הליך פעולה אפשרי:

-   מחקר על אנשים או משפחות שלמענם יונחו אבני נגף: אם ידועים השמות ומקום
    המגורים, התרכזו במחקר בבנות או בני מפשחה נוספים או שכנות/ים. אבל
    אפשר גם ליזום פרויקט אבני נגף בנוגע לתולדות בית דירות אחד או יותר
    בימי הנאציזם. לשם כך תוכלו להסתייע בספר הזיכרון ״קורבנות רדיפת
    היהודים תחת הדיקטטורה הנאצית בגרמניה 1933 -- 1945״. מאגר המידע הזה
    מכיל שמות ופרטים של נרדפות/ים יהודיות/ים. השם ומקום המגורים של האדם
    הנרדף יכולים לשמש נקודת מוצא למחקר. ואולם, בספר הזיכרון אין כתובות
    ספציפיות. גישה אל התיקים ואל הכתובות תמצאו בארכיון המדינה הראשי של
    ברנדנבורג או בארכיונים ומוזיאונים מקומיים.
-   מחקר בישוב עצמו: ראשית כל חפשו מידע בסביבת המגורים הקרובה. שאלו על
    קרובות וקרובי משפחה, עדות או עדים מהתקופה, שכנות/ים בבניין וברחוב.
-   מחקר בפרסומים נגישים לציבור: פנו למוזאוני מולדת, אגודות להיסטוריה,
    וכן לספריות ממלכתיות, מחוזיות ועירוניות.
-   מחקר מקוון: התחילו בשאלות במנועי חיפוש. כאן יכול לעזור לעתים קרובות
    עיקרון כדור השלג, כלומר חפשו לפי מילות מפתח כמו ישובים, שמות או
    שנים. סיקרו קטלוגים מקוונים של ספריות, במיוחד באוספי כתבי העת או
    בחומרים הנגישים אונליין. בנוגע לאדם מסוים חפשו כתובות מגורים ונשות
    או אנשי קשר.
-   ארכיונים מקומיים: נסו למצוא מסמכים בארכיון עצמו. סיקרו למשל תעודות
    של מנהלת העיר, של הקהילה הכנסייתית, של הקהילה היהודית, של משרדי
    הרישום ושל אגודות להיסטוריה.
-   אתרי הנצחה: באתרי הנצחה רבים של מחנות ריכוז יש מאגרי מידע שאינם
    פתוחים לציבור הרחב, ובהם מידע על אסירות ואסירים. אם ידוע לכן/ם באיזה
    מחנה ריכוז האדם היה אסיר, באפשרותכן/ם לפנות ישירות לאתר ההנצחה.

מקומות אחרים לבקשת סיוע, כגון [יוזמות מקומיות להנחת אבני נגף
](https://www.stolpersteine-brandenburg.de/de/aktiv_werden/lokale_initiativen.html)וכן
[ארכיונים ואתרי
הנצחה](https://www.stolpersteine-brandenburg.de/de/hintergrund/archive_und_gedenkstaetten.html)
תמצאו באתר הזה.
