---
title: ניקוי אבנים
category: 2 להיות פעיל
category_de: aktiv werden
order: 50
---

# ניקוי אבנים

<img src="{{ "/assets/images/steine/stolperstein-putzen-hermann-jacks.jpg" | relative_url }}" alt="ניקוי אבנים Hermann Jacks" class="float-left w-25 mr-3 mt-3">

טיפול באבני הנגף שהונחו הוא חלק חשוב בהנצחה בדיוק כמו המחקר על פרטי האדם
המונצח. בעניין זה יש לנו כמה עצות, איך כדאי לגשת בצורה נכונה לטיפול הזה.
את האבנים יש לנקות בקביעות מפני שלוחית הפליז עלולה לדהות עם הזמן בגלל
תנאי מזג האוויר, והכיתוב עלול להיעשות בגלל זה בלתי קריא. ואולם יוזמות
אחדות ממליצות לא לנקות בצורה אינטנסיבית מדי, מפני שהדבר עלול לפגום בשכבת
החימצון, והכיתוב עלול להיעשות בלתי קריא בגלל הנזק לחומר.

בשביל ניקוי כללי אפשר להשתמש במים וספוג לא שורט. בשביל ניקוי יסודי יותר
מומלץ להשתמש בחומר לניקוי מתכות שניתן לקנות בחנויות, כמו למשל של
היצרניות Sidol או Elsterglanz. למניעת שוליים לבנים על המדרכה המקיפה את
האבן יש למרוח בספוג או במטלית כמות מזערית של חומר הניקוי על לוחית הפליז.
אחרי זמן פעולה של כדקה יש לצחצח את הלוחית במטלית יבשה. אם יש על הלוחית
כתמים עקשניים, יש לחזור על ההליך. אנא אל תשתמשו באמצעי עזר בעלי פני שטח
קשיחים כמו מברשות מתכת או ברזלית לסירים, אשר עלולים להסב נזק ללוחית
הפליז. להלן שני סרטונים מברנדנבורג עם הוראות לניקוי ובעיות אפשריות.
יוזמות ניקוי משותפות יכולות לתרום לשימור תרבות של הנצחה וזיכרון. לרוב
מתאימים לכך ימי הזיכרון ב-9 בנובמבר, התאריך המרכזי של פוגרום נובמבר,
וב-27 בינואר, יום השחרור של מחנה הריכוז אושוויץ-בירקנאו ויום הזיכרון
הבינלאומי לקורבנות הנאציזם. כאן אפשר, לרגל יום הזיכרון, להניח פרחים
ולהדליק נרות אחרי הניקוי, להקריא את השמות ואת הביוגרפיות או להניח אותם
במקום.

[ניקוי אבני נגף -- ניסוי, תעייה והצלחה
](https://www.youtube.com/watch?v=x1BjHU4fBFc)[(1:27)](https://www.youtube.com/watch?v=x1BjHU4fBFc)

[כיצד לנקות אבני נגף?
](https://www.youtube.com/watch?v=sOa8cTHNhO0)[(1:25)](https://www.youtube.com/watch?v=sOa8cTHNhO0)
