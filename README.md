# Stolpersteine Brandenburg

Stolpersteine Brandenburg website using GitLab Pages. View it live at https://www.stolpersteine-brandenburg.de/

Stolpersteine Brandenburg uses [Jekyll](https://jekyllrb.com/) to convert the files in this repository to a HTML website.

The pages are written in a simple text format called Mardown.

> Markdown is a text-to-HTML conversion tool for web writers.
> Markdown allows you to write using an easy-to-read, easy-to-write plain text format,
> then convert it to structurally valid XHTML (or HTML).

https://daringfireball.net/projects/markdown/

Specifically, Jekyll  uses "GitHub Flavored Markdown", this is converted to the HTML shown on the website.
To find out how to use it see here: https://github.github.com/gfm/

## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Jekyll
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Add content

The above commands should be executed from the root directory of this project.

## GitLab CI

Git repository is hosted on: https://gitlab.com/stolpersteine-site-brandenburg/website

Build status is: https://gitlab.com/stolpersteine-site-brandenburg/website/-/pipelines

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: ruby:latest

variables:
  JEKYLL_ENV: production

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```
