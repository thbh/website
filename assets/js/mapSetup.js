window.mapSetup = {
  tileLayer: L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'eikes/civnq3kny002j2joi3d16t0kn',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZWlrZXMiLCJhIjoiYTJRQ1JsMCJ9.M8P2KYfdrKAn0OnFRrhCiQ'
  }),
  iconUrl: $("#stolperstein-svg").attr('href'),
  options: { tap: false }
};
window.mapSetup.icon = L.icon({
  iconUrl: window.mapSetup.iconUrl,
  iconSize: [20, 20],
  iconAnchor: [10, 20],
  popupAnchor: [0, -20]
});
