window.i18n = {
  "locales": {
    "de": {
      "FullName": "Name",
      "bornDate": "Geburtsdatum",
      "diedDate": "Todesdatum",
      "street": "Strasse",
      "postalCode": "Postleitzahl",
      "city": "Stadt",
      "map": "Karte",
      "place": "Ort",
      "persecution": "Verfolgung",
      "biographyHTML": "Biografie",
      "inscription": "Inschrift",
      "image": "Bild",
      "close": "Schließen"
    },
    "en": {
      "FullName": "Name",
      "bornDate": "Date of birth",
      "diedDate": "Date of death",
      "street": "Street",
      "postalCode": "Postal Code",
      "city": "City",
      "map": "Map",
      "place": "Place",
      "persecution": "Persecution",
      "biographyHTML": "Biography",
      "inscription": "Inscription",
      "image": "Image",
      "close": "Close"
    },
    "he": {
      "FullName": "שֵׁם",
      "bornDate": "תאריך לידה",
      "diedDate": "תאריך פטירה",
      "street": "רְחוֹב",
      "postalCode": "מיקוד",
      "city": "עִיר",
      "map": "מַפָּה",
      "place": "אֲתַר",
      "persecution": "סיבה לרדיפה",
      "biographyHTML": "ביוגרפיה",
      "inscription": "כְּתוֹבֶת",
      "image": "תמונה",
      "close": "סגור"
    }
  },
  current: function(key) {
    return window.i18n.locales[document.documentElement.lang];
  },
  t: function(key) {
    return window.i18n.current()[key] || key;
  }
};
