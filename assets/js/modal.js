$(function() {
  var modalWrapper = document.getElementById('modal-wrapper'),
      $modalDialog = $(modalWrapper.children[0]),
      modal = new bootstrap.Modal(modalWrapper),
      map = undefined;

  modalWrapper.addEventListener('hidden.bs.modal', function (e) {
    if (map && map.remove) {
      map.off();
      map.remove();
    }
    window.location.hash = '';
  });

  window.showStolpersteinModal = function(stolperstein) {
    var modalDialogContent = window.renderMustacheTemplate("modal", stolperstein.properties);
    $modalDialog.html(modalDialogContent);

    modal.show();

    var latLng = Array.from(stolperstein.geometry.coordinates).reverse();
    map = L.map('popup-map', window.mapSetup.options);
    window.mapSetup.tileLayer.addTo(map);

    var marker = L.marker(latLng, { icon: window.mapSetup.icon });
    marker.addTo(map);
    map.setView(latLng, 13);

    window.location.hash = stolperstein.properties.id;
  };

});
