$(function() {

  if (!document.getElementById('map')) return;

  var map = L.map('map', mapSetup.options);
  map.fitBounds([[51.51764, 11.73562], [53.31893, 14.71912]]);

  mapSetup.tileLayer.addTo(map);

  var iconCreateFunction = function (cluster) {
    var childCount = cluster.getChildCount();
    var html = '<div style="background-image:url(' + mapSetup.iconUrl + ')"><span>' + childCount + '</span></div>';
    var h = Math.floor(Math.log10(childCount));
    var size = 20 + h * 10;
    return new L.DivIcon({
      html:        html,
      className:   'stolperstein-cluster stolperstein-cluster-' + h,
      iconSize:    [size, size],
      iconAnchor:  [size/2, size],
      popupAnchor: [0, -size]
    });
  };

  var clusterGroup = L.markerClusterGroup({
    singleMarkerMode:           false,
    showCoverageOnHover:        false,
    zoomToBoundsOnClick:        true,
    removeOutsideVisibleBounds: true,
    iconCreateFunction:         iconCreateFunction
  });

  var popupMinWidth = 600,
      popupMaxWidth = 800,
      windowWidth = $(window).width(),
      windowHeight = $(window).height();
  if (windowWidth < popupMinWidth) { popupMinWidth = windowWidth; }
  if (windowWidth < popupMaxWidth) { popupMaxWidth = windowWidth; }

  var renderPopupContent = function(layer) {
    return window.renderMustacheTemplate("popup", layer.feature.properties);
  }

  function onEachFeature(feature, layer) {
    layer.bindPopup(renderPopupContent, {
      minWidth: popupMinWidth,
      maxWidth: popupMaxWidth - 60,
      maxHeight: windowHeight - 200
    });
  };

  function pointToLayer(feature, latlng) {
    return L.marker(latlng, {
      icon: mapSetup.icon
    });
  };

  function addGeoJsonToMap(data) {
    var geoJsonAttributes = {
      onEachFeature: onEachFeature,
      pointToLayer:  pointToLayer
    };
    var geojson = L.geoJSON(data, geoJsonAttributes);
    clusterGroup.addLayer(geojson);
    map.addLayer(clusterGroup);
  };

  fetch(window.api_url).then(response => response.json()).then(addGeoJsonToMap);

});
