$(function() {

  if (!document.getElementById('home-slider')) return;

  var stolpersteine;

  var newRandomInscription = function() {
      var visible_stolpersteine = $('#home-slider .stolperstein:visible'),
          random_visible_stolpersteine_index = Math.floor(Math.random() * visible_stolpersteine.length),
          random_stolpersteine_index = Math.floor(Math.random() * stolpersteine.length),
          stolperstein = stolpersteine[random_stolpersteine_index],
          $element = $(visible_stolpersteine[random_visible_stolpersteine_index]);
      $element.fadeOut(1000, function() {
        $element.data('stolperstein', stolperstein);
        $element.html(stolperstein.properties.inscription);
        $element.fadeIn(1000);
      });
      window.setTimeout(newRandomInscription, 10000);
  }

  var viewInscriptionsInSlider = function(data) {
    stolpersteine = $.grep(data.features, function(stolperstein) {
      return stolperstein.properties.inscription && stolperstein.properties.inscription.length > 0;
    });
    $('#home-slider .stolperstein').each(function(index, element) {
      var random_index = Math.floor(Math.random() * stolpersteine.length),
          stolperstein = stolpersteine[random_index],
          $element = $(element);
      $element.data('stolperstein', stolperstein);
      $element.html(stolperstein.properties.inscription);
      $element.removeClass('d-none');
    });
    window.setTimeout(newRandomInscription, 5000);

    if (window.location.hash) {
      var stolperstein_id = window.location.hash.replace('#', '');
      var stolperstein = data.features.find(stolperstein => stolperstein.properties.id == stolperstein_id);
      if (stolperstein) {
        window.showStolpersteinModal(stolperstein);
      }
    }
  };

  $.getJSON(window.api_url, viewInscriptionsInSlider);

  $(document).on('click', '#home-slider .stolperstein', function(event) {
    var stolperstein = $(event.target).data('stolperstein');
    window.showStolpersteinModal(stolperstein);
    event.preventDefault();
  });

});
