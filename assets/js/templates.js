$(function() {
  var templates = {
    "modal": $('#template-modal').html(),
    "popup": $('#template-popup').html(),
    "body": $('#template-body').html()
  }

  window.renderMustacheTemplate = function(template_name, stolperstein_properties) {
    var mustacheView = {
      "api_base_url": window.api_base_url,
      "properties":   stolperstein_properties,
      "i18n":         window.i18n.current()
    };
    return Mustache.render(templates[template_name], mustacheView, templates);
  };
});
