---
layout: none
---

{% include_relative bootstrap/javascripts/bootstrap.min.js %}
{% include_relative jquery/jquery-3.3.1.min.js %}
{% include_relative leaflet/leaflet.js %}
{% include_relative vendor/mustache.js %}
{% include_relative vendor/leaflet.markercluster-src.js %}
{% include_relative datatables/datatables.min.js %}
{% include_relative vendor/datatables-translations.js %}
{% include_relative vendor/dataTables.bootstrap5.min.js %}
window.api_base_url = 'https://stst-site.aktionsbuendnis-brandenburg.de';
window.api_url = `${window.api_base_url}/api/`;
{% include_relative js/i18n.js %}
{% include_relative js/mapSetup.js %}
{% include_relative js/templates.js %}
{% include_relative js/modal.js %}
{% include_relative js/karte.js %}
{% include_relative js/liste.js %}
{% include_relative js/home.js %}
