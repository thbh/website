---
title: Cleaning the Stolpersteine
category: 2 Becoming active
order: 50
---

# Cleaning the Stolpersteine

<img src="{{ "/assets/images/steine/stolperstein-putzen-hermann-jacks.jpg" | relative_url }}" alt="Stolperstein putzen Hermann Jacks" class="float-right w-25 ml-3 mt-3">

The upkeep of the Stolpersteine is just as important a part of
commemoration as researching the personal data. Here are a few pointers
on how to proceed with their maintenance. The stones should be cleaned
regularly as the brass plate can tarnish over time due to oxidation and
the inscription becomes illegible. However, some Stolpersteine
initiatives advise against excessive cleaning as this can damage the
oxidation layer. So the inscription might become illegible due to the
removal of the material.

For a light cleaning you can use a non-scratching sponge and water. For
a more thorough cleaning there are commercial metal polishes available,
like the ones from \"Sidol\" or \"Elsterglanz\". To avoid white edges on
the surrounding pavement, the cleaning agent should be spread on the
brass plate in small quantities with either a sponge or a cloth. After
about one minute the plate should be polished with a dry cloth. In case
of tougher stains you can repeat the procedure. Please do not use any
tools with a very abrasive surface such as wire brushes or scouring
pads, as they will damage the brass plates. Below are two videos from
Brandenburg with cleaning instructions and possible problems. Joint
maintenance campaigns by Stolperstein initiatives can help keep alive
the culture of commemoration and remembrance. The 9 November, the key
date of the November pogroms, and 27 January, the liberation day of the
Auschwitz-Birkenau concentration camp and the International Day of
Remembrance for the Victims of National Socialism, are suitable for
communal commemoration. Apart from cleaning the stones, flowers and
candles can also be placed beside them, names and biographies can be
read or left at the Stolpersteine, as well.

Cleaning Stolpersteine - trial, error and success (1:27)

How to clean a Stolperstein? (1:25)
