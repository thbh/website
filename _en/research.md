---
title: Research
category: 2 Becoming active
order: 40
---

# Research

<img src="{{ "/assets/images/steine/stolpersteine-bei-der-verlegung.jpg" | relative_url }}" alt="Gunter Demnig bei der Verlegung" class="w-50 float-right ml-3 mt-3">

At the beginning of the Stolperstein project there is a lot of research
to be done. Keep in mind that Stolpersteine is an art project that deals
with active commemoration. Historical research for the Stolperstein
should go beyond the collection of data and thus remind us of the life
of that particular person. Merely copying information from memorial
books is not what it is about. At the same time, it is not a scientific
project that aims to produce long and possibly incomprehensible texts.
It is meant to remind us of the stories and the lives of former local
citizens.

Try to initiate a conversation in the neighbourhood. Approach former and
current residents, local associations, religious communities, political
parties, schools, training institutions, colleges and universities. Seek
contact with local historians, historical societies and academics of
historical or specifically Judaic studies at regional universities.

Make enquiries about:

-   People: family members, friends, classmates, work colleagues
-   Places of residence: the street, the flat in the house, other
    addresses in case of moves, changes to street names and layout of
    some areas
-   Family: births, baptisms, weddings, anniversaries, deaths, name
    changes
-   Education and career: school and professional qualifications,
    occupations, academic studies
-   Events: changes after 1933, response to National Socialist politics
    in the community

When researching you should start on site and then go from there. If
possible, interview contemporary witnesses or their descendants. One
possible approach could be:

-   **Identify individuals and families for whom a Stolperstein is to be
    set:** If the names and the place of residence are already known,
    the research focuses on further family members or house residents.
    However, it is also possible to initiate a Stolperstein project by
    researching the history of one or several houses during the National
    Socialist era. On the website of the Federal Archives
    (http://www.bundesarchiv.de/gedenkbuch) you can find the names of
    persecuted Jews. You can begin researching here, by name or place of
    residence. However, no specific addresses are given on this website.
    Access to the files and the addresses can be obtained through the
    Brandenburg State Archives or through local archives or museums.
-   **On-site investigation:** First look for information in the
    immediate vicinity of the place of residence. Ask for family
    members, contemporary witnesses and former residents of the house
    and the street.
-   **Research in available publications:** Contact local history
    museums, historical societies and the municipal, state and national
    libraries.
-   **Online research:** Start by consulting search engines. Look in the
    online catalogues of the libraries, especially in the journal
    collections or in the available online resources. Search for
    addresses and contact persons.
-   **Local archives:** Try to find documents locally. For example,
    search through materials from the city administration, the church
    community, the Jewish community, the registry offices and historical
    societies.

For contact information on archives and memorial sites check out this
website.

You are responsible for the results of your research and the handling of
personal data. Therefore, make sure that your source references are
complete and check the data researched by others. This is important in
order to be able to answer questions at a later date. Furthermore, it
saves you from having to search again when you publish your research
results.
