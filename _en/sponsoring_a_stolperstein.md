---
title: Sponsoring a Stolperstein
category: 2 Becoming active
order: 20
---

# Sponsoring a Stolperstein

<img src="{{ "/assets/images/steine/stolperstein-familie-joel-putzen.jpg" | relative_url }}" alt="Stolperstein putzen Hermann Jacks" class="float-right w-25 ml-3 mt-3">

Producing, shipping and setting a Stolperstein currently costs about 120
euros. The stones are made in Berlin by the sculptor Michael
Friedrichs-Friedlaender. As most of the stones are laid by Gunter Demnig
himself, further costs may be involved. These include travel and
accommodation costs for him and his driver, depending on travel
distances. Gunter Demnig can be requested to make a presentation in
addition to the setting and if he agrees, it will cost an additional 200
euro plus VAT.

Due to the Corona pandemic it is currently not possible to set the
stones with Gunter Demnig in person, but they can still be shipped and
set at the site.

In order to finance the setting of a Stolperstein, many Stolperstein
initiatives set up accounts and ask for donations. It is possible to set
up sponsorship accounts for individual Stolpersteine. To get support for
your Stolperstein project, you can contact various local bodies such as
local businesses, associations and institutions, housing associations,
banks or foundations and ask these institutions to sponsor your project.
Of course, individuals can also take on one or more sponsorships. The
sponsor bears all or part of the costs for the production and setting of
a Stolperstein. There are also shared sponsorships for one or more
stones.
