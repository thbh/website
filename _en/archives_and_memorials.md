---
title: Archives and memorials
category: 3 Background
order: 50
---

# Archives and memorials

There are various sources that can be used to research the biographies
of victims of political persecution. Below you will find an overview of
archives, databases and memorial sites in Brandenburg that can help you
with your research.

## Brandenburg Main State Archives

For a search in the Main State Archives you can submit an informal
written request in which you state your research question. You will
receive the search result after about six weeks. If necessary, you can
then arrange for an appointment to view the files in the reading room.
If the research is part of an educational project, you should examine
the files first to find out about the relevance of the source.The
content of the files can be emotionally overwhelming and therefore their
examination should be carefully prepared for.

**Contact**

Brandenburgisches Landeshauptarchiv \
Am Windmühlenberg 3 \
14469 Potsdam

E-mail: <poststelle@blha.brandenburg.de> \
Web: <https://blha.brandenburg.de/> \
Phone: +49 (0) 331 5674-0 \
Fax: +49 (0)331 5674-212

Contact person: Dr. Monika Nakath

## Federal Archives

Numerous documents containing personal data from the time of the Nazi
regime are kept in the Federal Archives. For a search you must submit an
informal written request stating the subject and the purpose of your
enquiry. Research and consultation are always conducted on the premises
of the Federal Archives. Minor queries can be handled by the staff of
the Federal Archives. The result will be communicated to you in writing.
This process may be subject to fees.

**Contact**

Bundesarchiv \
Finckensteinallee 63 \
12205 Berlin

E-mail: <berlin@bundesarchiv.de> \
Web: <https://www.bundesarchiv.de/>

Phone: +49 (0)30 187770-0 \
Fax: +49 (0)30 187770-111

Department of archive services

Phone: +49 (0)30 187770-420 or -4111

## International Tracing Service Bad Arolsen

You can submit a written application for scientific research to the
International Tracing Service. To do so, use the form on their website.
The research request can be related to a specific person, but also to a
certain topic. In the location-based search, you can search for places,
districts, companies or other similar information. If the search service
has any entries, you will get the information free of charge after
signing the user statement. A fee will be charged for sending documents:
€ 0,30 to € 0,90 per copy and € 5,- for a DVD containing the
information.

**Contact**

Internationaler Suchdienst Bad Arolsen \
Große Allee 5-9 \
34454 Bad Arolsen \
Web: <https://arolsen-archives.org/>

Phone: +49 (0)5691 629-0 \
Fax: +49 (0)5691 629-501

## Yad Vashem

The Holocaust Martyrs' and Heroes' Remembrance Authority "Yad
Vashem" is a memorial site in Israel that commemorates and
scientifically documents the Nazi extermination of Jews. The website
contains a database with names and biographical details of 3.6 million
Holocaust victims, which you can search online. You also have access to
the photo archive and various directories, such as transport
directories, information on Jews who chose to seek suicide rather than
deportation, and directories of survivors from the concentration camps.
This database contains the largest collection of Holocaust related
publications that can be accessed online.

**Contact**

Yad Vashem \
P.O.B. 3477 \
Jerusalem 91034 \
Israel \
Web: <https://www.yadvashem.org/>

For enquiries: International Institute for Holocaust Research \
E-Mail: <research.institute@yadvashem.org.il> \
Phone: +972 2 6443480 or +972 2 6443479

Since many people from Brandenburg also lived in Berlin for a certain
period of time you can use the research guide of the Berlin
Co-ordination Office for the Stolpersteine to access databases and
archives for information on these people.

## Memorials in Brandenburg

Memorial for the Victims of Political Violence in the 20th Century
Lindenstraße 54/55

In its permanent exhibition, the Lindenstraße Memorial deals with the
continuity of political persecution. The multimedia exhibition guides
the visitor through different periods of time, focussing on persecution
during the Nazi era, during the Soviet occupation and at the time of the
GDR.

In 1935 the Nazis set up a "Hereditary Health Court" in this house
with the task of deciding on forced sterilisations. This court gave
forced sterilisations a legal standing. After the People' s Court was
relocated from Berlin to Potsdam in 1939 the house in Lindenstraße was
used as a remand centre for political prisoners. A large number of
members of various resistance groups were detained here, sentenced and
later executed at other locations.

In 1953 the compound was taken over by the GDR's State Security and
continued to function as a remand centre.

**Contact**

Lindenstraße 54/55 \
14467 Potsdam \
Web: <https://www.gedenkstaette-lindenstrasse.de/>

## Memorial and museum Sachsenhausen in Oranienburg

The Sachsenhausen concentration camp was established by the Nazis in 1936.
It is located in Oranienburg just north of Berlin. The camp served
as a training site for concentration camp commanders and was used to
house an SS contingent. From 1936 to 1945 approximately 200,000 people
were deported to Sachsenhausen. Beginning in August 1941 the Nazis
murdered between 13,000 and 18,000 Russian prisoners of war at this site
in ten weeks.

Due to the Red Army's advance in spring 1945, the camp was evacuated by
the SS on 21 April 1945, and the remaining prisoners were forced to
embark on a death march. Polish and Russian units reached the camp on 22
and 23 April and liberated it. In August 1945, following the
Sachsenhausen concentration camp, the Soviet Allies converted the site
into a "Soviet Special Camp". The "Soviet Special Camp" was closed
in 1950.

From 1962 until the reunification of Germany it was one of the three
"National Memorial Sites of the GDR". Since 1991 the memorial has been
run by the Brandenburg Memorials Foundation.

**Contact**

Straße der Nationen 22 \
16515 Oranienburg \
Web: <https://www.sachsenhausen-sbg.de/>

## Memorial to the Victims of Euthanasia Murders in Brandenburg an der Havel

Since 2012 a permanent exhibition at the Brandenburg an der Havel
memorial has commemorated the over 9,000 people who were murdered by the
Nazis at this location between January and August 1940 as part of the
"Euthanasia programme". The Brandenburg "killing centre" was one of
six sites in the German Reich where more than 70,000 mentally ill and
handicapped people were murdered as part of "Aktion T4".

**Contact**

Nicolaiplatz 28 \
14770 Brandenburg an der Havel \
Web: <https://www.brandenburg-euthanasie-sbg.de/>

## Ravensbrück Memorial Site

The former concentration camp Ravensbrück was the largest concentration
camp for women. It was built by the SS in December 1938 and liberated by
the Red Army in April 1945. During this period an estimated 28,000
prisoners were murdered here. In 1959 the "Ravensbrück National
Memorial Site" was built here, and since 1993 has been run by the
Brandenburg Memorials Foundation. Since 2013 the permanent exhibition
"The Ravensbrück Women's Concentration Camp - History and
Remembrance" as well as smaller more detailed exhibitions may be
visited here.

**Contact**

Straße der Nationen \
16798 Fürstenberg/Havel \
Web: <https://www.ravensbrueck-sbg.de/>
