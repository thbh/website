---
layout: doctype
title: Map
category: 1 Stolpersteine discovery
order: 5
---

<html lang="{{ page.lang | default: site.lang | default: "en" }}" class="h-100">

  {%- include head.html -%}

  <body class="h-100">

    <div class="d-flex flex-column h-100">
      {%- include navigation.html -%}

      <div class="w-100 flex-grow-1" id="map"></div>
    </div>

  </body>

</html>
